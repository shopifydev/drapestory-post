exports.getPagingData = (total_record, per_page_record, page_no) => {
  const totalPages = Math.ceil(total_record / per_page_record) || 0;
  const pageData = {
    total_pages: totalPages,
    page: page_no,
    total: total_record,
    limit: per_page_record,
    next_page: page_no < totalPages,
    pervious_page: page_no > 1
  };
  return pageData;
};


exports.getCurrencySymbols = (symbol) => {
  try {
    let currecySymbol = {
      'AED': 'د.إ', // ?
      'AFN': 'Af',
      'ALL': 'Lek',
      'AMD': '',
      'ANG': 'ƒ',
      'AOA': 'Kz', // ?
      'ARS': '$',
      'AUD': '$',
      'AWG': 'ƒ',
      'AZN': 'мaн',
      'BAM': 'KM',
      'BBD': '$',
      'BDT': '৳', // ?
      'BGN': 'лв',
      'BHD': '.د.ب', // ?
      'BIF': 'FBu', // ?
      'BMD': '$',
      'BND': '$',
      'BOB': '$b',
      'BRL': 'R$',
      'BSD': '$',
      'BTN': 'Nu.', // ?
      'BWP': 'P',
      'BYR': 'p.',
      'BZD': 'BZ$',
      'CAD': '$',
      'CDF': 'FC',
      'CHF': 'CHF',
      'CLF': '', // ?
      'CLP': '$',
      'CNY': '¥',
      'COP': '$',
      'CRC': '₡',
      'CUP': '⃌',
      'CVE': '$', // ?
      'CZK': 'Kč',
      'DJF': 'Fdj', // ?
      'DKK': 'kr',
      'DOP': 'RD$',
      'DZD': 'دج', // ?
      'EGP': '£',
      'ETB': 'Br',
      'EUR': '€',
      'FJD': '$',
      'FKP': '£',
      'GBP': '£',
      'GEL': 'ლ', // ?
      'GHS': '¢',
      'GIP': '£',
      'GMD': 'D', // ?
      'GNF': 'FG', // ?
      'GTQ': 'Q',
      'GYD': '$',
      'HKD': '$',
      'HNL': 'L',
      'HRK': 'kn',
      'HTG': 'G', // ?
      'HUF': 'Ft',
      'IDR': 'Rp',
      'ILS': '₪',
      'INR': '₹',
      'IQD': 'ع.د', // ?
      'IRR': '﷼',
      'ISK': 'kr',
      'JEP': '£',
      'JMD': 'J$',
      'JOD': 'JD', // ?
      'JPY': '¥',
      'KES': 'KSh', // ?
      'KGS': 'лв',
      'KHR': '៛',
      'KMF': 'CF', // ?
      'KPW': '₩',
      'KRW': '₩',
      'KWD': 'د.ك', // ?
      'KYD': '$',
      'KZT': 'лв',
      'LAK': '₭',
      'LBP': '£',
      'LKR': '₨',
      'LRD': '$',
      'LSL': 'L', // ?
      'LTL': 'Lt',
      'LVL': 'Ls',
      'LYD': 'ل.د', // ?
      'MAD': 'د.م.', //?
      'MDL': 'L',
      'MGA': 'Ar', // ?
      'MKD': 'дeн',
      'MMK': 'K',
      'MNT': '₮',
      'MOP': 'MOP$', // ?
      'MRO': 'UM', // ?
      'MUR': '₨', // ?
      'MVR': '.ރ', // ?
      'MWK': 'MK',
      'MXN': '$',
      'MYR': 'RM',
      'MZN': 'MT',
      'NAD': '$',
      'NGN': '₦',
      'NIO': 'C$',
      'NOK': 'kr',
      'NPR': '₨',
      'NZD': '$',
      'OMR': '﷼',
      'PAB': 'B/.',
      'PEN': 'S/.',
      'PGK': 'K', // ?
      'PHP': '₱',
      'PKR': '₨',
      'PLN': 'zł',
      'PYG': 'Gs',
      'QAR': '﷼',
      'RON': 'lei',
      'RSD': 'Дин.',
      'RUB': 'py6',
      'RWF': 'ر.س',
      'SAR': '﷼',
      'SBD': '$',
      'SCR': '₨',
      'SDG': '£', // ?
      'SEK': 'kr',
      'SGD': '$',
      'SHP': '£',
      'SLL': 'Le', // ?
      'SOS': 'S',
      'SRD': '$',
      'STD': 'Db', // ?
      'SVC': '$',
      'SYP': '£',
      'SZL': 'L', // ?
      'THB': '฿',
      'TJS': 'TJS', // ? TJS (guess)
      'TMT': 'm',
      'TND': 'د.ت',
      'TOP': 'T$',
      'TRY': '₤', // New Turkey Lira (old symbol used)
      'TTD': '$',
      'TWD': 'NT$',
      'TZS': '',
      'UAH': '₴',
      'UGX': 'USh',
      'USD': '$',
      'UYU': '$U',
      'UZS': 'лв',
      'VEF': 'Bs',
      'VND': '₫',
      'VUV': 'VT',
      'WST': 'WS$',
      'XAF': 'FCFA',
      'XCD': '$',
      'XDR': '',
      'XOF': '',
      'XPF': 'F',
      'YER': '﷼',
      'ZAR': 'R',
      'ZMK': 'ZK', // ?
      'ZWL': 'Z$',
    }
    let currencyResponse = 'amount'
    if (currecySymbol[symbol] != undefined) {
      currencyResponse = currecySymbol[symbol];
    }
    return currencyResponse
  } catch (err) {
    throw new Error('Unknown currency formate')
  }
}