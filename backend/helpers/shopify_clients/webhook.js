const CONFIG = require("../../config");
exports.webhookCreateObject = () => {
  let allWebhook = [];
  const webhookURL = CONFIG.SHOPIFY.WEBHOOK_URL
  let productCreate = {
    topic: "products/create",
    address: webhookURL,
    format: "json"
  }
  // allWebhook.push(productCreate)

  // let productUpdate = {
  //   topic: "products/update",
  //   address: webhookURL,
  //   format: "json"
  // }
  // allWebhook.push(productUpdate)

  let productsDelete = {
    topic: "products/delete",
    address: webhookURL,
    format: "json"
  }
  // allWebhook.push(productsDelete)

  let orderCreate = {
    topic: "orders/create",
    address: webhookURL,
    format: "json"
  }
  allWebhook.push(orderCreate)

  let orderUpdate = {
    topic: "orders/updated",
    address: webhookURL,
    format: "json"
  }
  // allWebhook.push(orderUpdate)

  let orderCancelled = {
    topic: "orders/cancelled",
    address: webhookURL,
    format: "json"
  }
  // allWebhook.push(orderCancelled)

  // let connectInventoryLevel = {
  //   topic: "inventory_levels/connect",
  //   address: webhookURL,
  //   format: "json"
  // }
  // allWebhook.push(connectInventoryLevel)

  let disconnectInventoryLevel = {
    topic: "inventory_levels/disconnect",
    address: webhookURL,
    format: "json"
  }
  // allWebhook.push(disconnectInventoryLevel)

  let createInventoryItem = {
    topic: "inventory_items/create",
    address: webhookURL,
    format: "json"
  }
  // allWebhook.push(createInventoryItem)

  let deleteInventoryItem = {
    topic: "inventory_items/delete",
    address: webhookURL,
    format: "json"
  }
  // allWebhook.push(deleteInventoryItem)

  return allWebhook;
}

