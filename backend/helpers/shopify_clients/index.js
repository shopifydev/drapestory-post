require('@shopify/shopify-api/adapters/node')
const { shopifyApi, Session, ApiVersion } = require("@shopify/shopify-api");
const CONFIG = require('../../config');

exports.shopifyRestClient = ({ shop, accessToken }) => {
  console.log('obj',{
    apiKey: CONFIG.SHOPIFY.API_KEY,
    apiSecretKey: CONFIG.SHOPIFY.SECRET_KEY,
    apiVersion: ApiVersion.January23,
    scopes: [CONFIG.SHOPIFY.SCOPES],
    shop:shop,
    accessToken:accessToken,
    hostName: CONFIG.SHOPIFY.APP_URL.replace(/https?:\/\//, ""),
  })
  const Shopify = shopifyApi({
    apiKey: CONFIG.SHOPIFY.API_KEY,
    apiSecretKey: CONFIG.SHOPIFY.SECRET_KEY,
    apiVersion: ApiVersion.January23,
    scopes: [CONFIG.SHOPIFY.SCOPES],
    hostName: CONFIG.SHOPIFY.APP_URL.replace(/https?:\/\//, ""),
  });
  const session = new Session({
    id: Shopify.auth.nonce(),
    shop: shop,
    state: Shopify.auth.nonce(),
    isOnline: false,
    accessToken: accessToken,
  });
  const client = new Shopify.clients.Rest({ session: session });
  return client;
}

exports.shopifyGraphqlClient = ({ shop, accessToken }) => {
  const Shopify = shopifyApi({
    apiKey: CONFIG.SHOPIFY.API_KEY,
    apiSecretKey: CONFIG.SHOPIFY.SECRET_KEY,
    apiVersion: ApiVersion.January23,
    scopes: [CONFIG.SHOPIFY.SCOPES],
    hostName: CONFIG.SHOPIFY.APP_URL.replace(/https?:\/\//, ""),
    isEmbeddedApp: true,
    adminApiAccessToken: accessToken,
  });
  const session = new Session({
    id: Shopify.auth.nonce(),
    shop: shop,
    state: Shopify.auth.nonce(),
    isOnline: false,
    accessToken: accessToken,
  });
  const client = new Shopify.clients.Graphql({ session: session });
  // let data = await client.query({
  //     data: schema
  // })
  return client;
}

exports.shopifyStorefrontClient = ({ shop, token, storefrontToken }) => {
  const Shopify = shopifyApi({
    apiKey: CONFIG.SHOPIFY.API_KEY,
    apiSecretKey: CONFIG.SHOPIFY.SECRET_KEY,
    apiVersion: ApiVersion.January23,
    scopes: [CONFIG.SHOPIFY.SCOPES],
    hostName: CONFIG.SHOPIFY.APP_URL.replace(/https?:\/\//, ""),
    isEmbeddedApp: true,
    adminApiAccessToken: token,
    privateAppStorefrontAccessToken: storefrontToken
  })

  const session = new Session({
    id: Shopify.auth.nonce(),
    shop: shop,
    state: Shopify.auth.nonce(),
    isOnline: false,
    accessToken: token,
  });

  const client = new Shopify.clients.Storefront({
    session: session
  });

  return client;
}