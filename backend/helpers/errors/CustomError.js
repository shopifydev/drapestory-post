const ResponseCodes = require("./ResponseCodes")

class CustomError extends Error {
  static SUCCESS = 200
  static ACCEPTED = 201
  static BAD_REQUEST = 400
  static PAYMENT_REQUIRED = 402
  static ACCESS_NOT = 403
  static NOT_FOUND = 404
  static NOT_ACCEPTABLE = 406
  static TIMEOUT = 408
  static LARGE_PAYLOAD = 413
  static SERVER_ERROR = 500
  static BAD_GATEWAY = 502

  constructor(statusCode = 500, message, redirectTo = undefined) {
    super();
    // assign the error class name in your custom error
    this.name = this.constructor.name

    if (!message) message = ResponseCodes.RESPONSE_CODES[statusCode].message || 'Something went wrong please try again'
    this.message = message
    this.code = statusCode // @info: error code for responding to client
    this.redirectTo = redirectTo // @info: if we want to redirect on some page when we get error
  }
}

module.exports = CustomError
