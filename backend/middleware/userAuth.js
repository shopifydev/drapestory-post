const jwt = require("jsonwebtoken");
const db = require("../models");
const CONFIG = require("../config");
const CustomError = require("../helpers/errors/CustomError");
const { RESPONSE_CODES } = require("../helpers/errors/ResponseCodes");

exports.authenticateToken = async (req, res, next) => {
  try {
    // console.log("Request ====",req)
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1];
    if (!token) {
      throw new CustomError(RESPONSE_CODES.BAD_REQUEST, "Required parameters missing");
    }
    let tokenPayload = token.split('.')[1]
    tokenPayload = JSON.parse(Buffer.from(tokenPayload, "base64").toString("utf8"))
    let domain = new URL(tokenPayload.dest).host
    let user = jwt.verify(token, CONFIG.SHOPIFY.SECRET_KEY);
    const shop = new URL(user.dest).host;
    if (!shop) throw new CustomError(RESPONSE_CODES.UNAUTHORIZED, "JWT is invalid.");
    let Store = await db.Store.findOne({
      where: { domain: shop },
    });
    if (!Store) throw new CustomError(RESPONSE_CODES.UNAUTHORIZED, `You don't have access for this connection`);
    req.storeData = Store;
    next();
  } catch (error) {
    console.log("authenticateToken----> ", error);
    console.log("authenticateToken----> ", req.originalUrl);
    // if (error instanceof jwt.TokenExpiredError) {
    //   error = new CustomError(302, "Redirect to login");
    // }
    next(new CustomError(RESPONSE_CODES.UNAUTHORIZED, "Unauthorized"));
  }
};

exports.authorizeDomains = async (req, res, next) => {
  try {
    const requestDomain = req.headers.domain;
    // console.log(requestDomain)
    if (!requestDomain) throw new CustomError(RESPONSE_CODES.UNAUTHORIZED, "Token is invalid.");
    let Store = await db.Store.findOne({
      where: { domain: requestDomain },
    });
    if (!Store) throw new CustomError(RESPONSE_CODES.UNAUTHORIZED, `You don't have access for this connection`);
    req.storeData = await JSON.parse(JSON.stringify(Store));
    next();
  } catch (error) {
    next(error)
  }
};