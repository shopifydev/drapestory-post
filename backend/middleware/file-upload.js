
const multer = require('multer');
const fs = require('fs')
if (!fs.existsSync(__dirname + '/uploads')) {
	fs.mkdirSync(__dirname + '/uploads');
}
const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, __dirname + '/uploads')
	},
	filename: (req, file, cb) => {
		cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
	}
});

const fileFilter = (req, file, cb) => {
	if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
		cb(null, true);
	} else {
		cb(new Error('Invalid file type. Only JPEG and PNG are allowed.'), false);
	}
};
exports.upload = multer({
	storage: storage,
	limits: { fileSize: 20 * 1024 * 1024 }, // 20 MB
	fileFilter: fileFilter
});

exports.uploadMiddleware = (req, res, next) => {
	upload.array('files', 5)(req, res, (err) => {
		if (err) {
			return next(err);
		}

		if (req.files.length > 5) {
			return next(new Error('You can upload a maximum of 5 files.'));
		}

		next();
	});
}
