const dotenv = require('dotenv');
dotenv.config();
dotenv.config({
    path: `./${process.env.NODE_ENV}.env`
});
const users = [{ id: 1, email: 'info@drapestory.in', password: 'drapestory@987' }]

const CONFIG = {
    NODE_ENV: process.env.NODE_ENV,
    PORT: process.env.PORT,
    SHOPIFY:{
        APP_URL: process.env.SHOPIFY_APP_URL,
        API_KEY: process.env.SHOPIFY_API_KEY,
        SECRET_KEY: process.env.SHOPIFY_SECRET_KEY,
        REDIRECT_URL:process.env.SHOPIFY_REDIRECT_KEY,
        SCOPES: process.env.SHOPIFY_SCOPES,
        WEBHOOK_URL: process.env.SHOPIFY_WEBHOOK_URL,
    },
    ALLOWED_DOMAINS: process.env.ALLOWED_DOMAINS,
    USERS: users,
    DB: {
        HOST: process.env.DB_HOST,
        PORT: process.env.DB_PORT,
        DATABASE: process.env.DB_DATABASE,
        USER: process.env.DB_USER,
        PASSWORD: process.env.DB_PASSWORD,
        DIALECT: process.env.DB_DIALECT,
        LOGS: process.env.DB_LOGS == true,
        POOL: {
            MAX: 5,
            MIN: 0,
            ACQUIRE: 30000,
            IDLE: 10000
        }
    },
}

module.exports = CONFIG