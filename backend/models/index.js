//Database Connection Build
const Sequelize = require("sequelize");
const CONFIG = require("../config");

const sequelize = new Sequelize(CONFIG.DB.DATABASE, CONFIG.DB.USER, CONFIG.DB.PASSWORD, {
  host: CONFIG.DB.HOST,
  dialect: CONFIG.DB.DIALECT,
  logging: CONFIG.DB.LOGS == 'true',
  port: CONFIG.DB.PORT,
  // dialectOptions: {
  //   useUTC: true,
  // },
  timezone: "+00:00",
  pool: {
    max: CONFIG.DB.POOL.MAX,
    min: CONFIG.DB.POOL.MIN,
    acquire: CONFIG.DB.POOL.ACQUIRE,
    idle: CONFIG.DB.POOL.IDLE
  }
});

const db = {};
db.sequelize = sequelize
//DB Connection

//All Models Include
db.Store = require("./store")(sequelize);
db.Customers = require('./customer')(sequelize)
db.Posts = require('./post')(sequelize)
db.InteriorDesign = require('./interior_design')(sequelize)
db.Products = require('./products')(sequelize)
db.Colors = require('./colors')(sequelize)
db.Moodboards = require('./moodboard')(sequelize)
db.order = require('./order')(sequelize)

db.Posts.belongsTo(db.Customers, { foreignKey: 'customer_id', sourceKey: 'id' })
db.Customers.hasMany(db.Posts, { foreignKey: 'customer_id', sourceKey: 'id' })

db.Moodboards.belongsTo(db.Customers, { foreignKey: 'customer_id', sourceKey: 'id' })
db.Customers.hasMany(db.Moodboards, { foreignKey: 'customer_id', sourceKey: 'id' })

module.exports = db;