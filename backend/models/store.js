const Sequelize = require('sequelize');

module.exports = (sequelize) => {
    return sequelize.define("store", {
        id: {
            type: Sequelize.INTEGER(11),
            primaryKey: true,
            autoIncrement: true,
        },
        store_id: {
            type: Sequelize.STRING(14),
            allowNull: false,
        },
        username: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING(320)
        },
        domain: {
            type: Sequelize.STRING
        },
        shop_created_at: {
            type: Sequelize.STRING
        },
        shop_owner: {
            type: Sequelize.STRING
        },
        token: {
            type: Sequelize.STRING(500),
        },
        storefront_token: {
            type: Sequelize.STRING(500),
        },
        iana_timezone: {
            type: Sequelize.STRING,
            defaultValue: null
        },
        guid: {
            type: Sequelize.STRING(20),
            allowNull: true,
            defaultValue: null

        },
        status: {
            type: Sequelize.DataTypes.ENUM('0', '1'),
            defaultValue: '0',
            allowNull: false,
            comment: '(0=inactive, 1 =active)'
        },
        is_deleted: {
            type: Sequelize.DataTypes.ENUM('0', '1'),
            defaultValue: '0',
            allowNull: false,
            comment: '(0=no,1=yes)'
        },
        currency: {
            type: Sequelize.STRING(10),
            defaultValue: null,
        },
        currency_format: {
            type: Sequelize.STRING(50) + ' CHARSET utf8 COLLATE utf8_general_ci',
            defaultValue: null,
        },
        tax_included: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        created_at: {
            type: Sequelize.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: true
        },
        updated_at: {
            type: Sequelize.DATE,
            allowNull: true,
            defaultValue: null
        }
    },
        {
            collate: 'utf8mb4_unicode_ci',
            timestamps: false
        });
}