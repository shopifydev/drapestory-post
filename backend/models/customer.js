const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define("customer", {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    first_name: {
      type: DataTypes.STRING(255),
      charset: 'utf8mb3',
      collate: 'utf8mb3_unicode_ci',
      defaultValue: null,
    },
    last_name: {
      type: DataTypes.STRING(255),
      charset: 'utf8mb3',
      collate: 'utf8mb3_unicode_ci',
      defaultValue: null,
    },
    phone: {
      type: DataTypes.STRING(14),
      collate: 'utf8mb4_general_ci',
      defaultValue: null,
    },
    email: {
      type: DataTypes.STRING(255),
      charset: 'utf8mb3',
      collate: 'utf8mb3_unicode_ci',
      defaultValue: null,
    },
    customer_id: {
      type: DataTypes.STRING(255),
      charset: 'utf8mb3',
      collate: 'utf8mb3_unicode_ci',
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null
    }
  },
    {
      collate: 'utf8mb4_unicode_ci',
      timestamps: false,
    }
  );
}