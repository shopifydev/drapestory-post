const { DataTypes, Sequelize } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define("post", {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    customer_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'customers', // name of Target model
        key: 'id',          // key in Target model that we're referencing
      },
    },
    image: {
      type: DataTypes.JSON,
      defaultValue: null
    },
    link: {
      type: DataTypes.STRING(255),
      charset: 'utf8mb3',
      collate: 'utf8mb3_unicode_ci',
      defaultValue: null,
    },
    title: {
      type: DataTypes.STRING(255),
      charset: 'utf8mb3',
      collate: 'utf8mb3_unicode_ci',
      defaultValue: null,
    },
    description: {
      type: DataTypes.STRING(255),
      charset: 'utf8mb3',
      collate: 'utf8mb3_unicode_ci',
      defaultValue: null,
    },
    status: {
      type: DataTypes.TINYINT(1),
      allowNull: false,
      defaultValue: 0,
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null
    },
  },
    {
      collate: 'utf8mb4_unicode_ci',
      timestamps: false,
      indexes: [
        {
          name: 'customer_id',
          fields: ['customer_id'],
        },
      ],
    });
}