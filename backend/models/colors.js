const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define("colors", {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    customer_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'customers',
        key: 'id',
      },
      defaultValue: null
    },
    name: {
      type: DataTypes.TEXT('medium'),
      charset: 'utf8mb4',
      collate: 'utf8mb4_unicode_ci',
    },
    code: {
      type: DataTypes.STRING(100),
      charset: 'utf8mb4',
      collate: 'utf8mb4_unicode_ci',
    },
    image: {
      type: DataTypes.STRING(100),
      charset: 'utf8mb4',
      collate: 'utf8mb4_unicode_ci',
      defaultValue: null
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: true
    }
  },
    {
      collate: 'utf8mb4_unicode_ci',
      timestamps: false,
      indexes: [
        {
          name: 'customer_id',
          fields: ['customer_id'],
        },
      ],
    });
}