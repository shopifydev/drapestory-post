const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
    return sequelize.define("order", {
        id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },
        order_id: {
            type: DataTypes.BIGINT,
            defaultValue: null
        },
        order_name: {
            type: DataTypes.TEXT('medium'),
            charset: 'utf8mb4',
            collate: 'utf8mb4_unicode_ci',
        },
        amount: {
            type: DataTypes.FLOAT(50, 2)
        },
        customer_email: {
            type: DataTypes.STRING(255),
            charset: 'utf8mb3',
            collate: 'utf8mb3_unicode_ci',
            defaultValue: null,
        },
        customer_name: {
            type: DataTypes.STRING(255),
            charset: 'utf8mb3',
            collate: 'utf8mb3_unicode_ci',
            defaultValue: null,
        },
        customer_id: {
            type: DataTypes.BIGINT,
            allowNull: true,
            // references: {
            //     model: 'customers',
            //     key: 'id',
            // },
            defaultValue: null
        },
        created_at: {
            type: DataTypes.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: true
        },

    },
    {
        collate: 'utf8mb4_unicode_ci',
        timestamps: false,
        indexes: [
          {
            name: 'customer_id',
            fields: ['customer_id'],
          },
        ],
      }
  
    );
}