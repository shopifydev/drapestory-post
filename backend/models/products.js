const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define("product", {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    product_id: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    title: {
      type: DataTypes.TEXT('medium'),
      charset: 'utf8mb4',
      collate: 'utf8mb4_unicode_ci',
    },
    handle: {
      type: DataTypes.TEXT('medium'),
      charset: 'utf8mb4',
      collate: 'utf8mb4_unicode_ci',
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null
    }
  },
    {
      collate: 'utf8mb4_unicode_ci',
      timestamps: false,
      indexes: [
        {
          name: 'product_id',
          fields: ['product_id'],
        },
      ],
    });
}