const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define("interior_design", {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    first_name: {
      type: DataTypes.STRING(255),
      collate: 'utf8mb4_general_ci',
      defaultValue: null,
    },
    last_name: {
      type: DataTypes.STRING(255),
      collate: 'utf8mb4_general_ci',
      defaultValue: null,
    },
    email: {
      type: DataTypes.STRING(255),
      collate: 'utf8mb4_general_ci',
      defaultValue: null,
    },
    phone: {
      type: DataTypes.STRING(14),
      collate: 'utf8mb4_general_ci',
      defaultValue: null,
    },
    gst_number: {
      type: DataTypes.STRING(15),
      collate: 'utf8mb4_general_ci',
      defaultValue: null,
    },
    location: {
      type: DataTypes.STRING(15),
      collate: 'utf8mb4_general_ci',
      defaultValue: null,
    },
    project_handle: {
      type: DataTypes.STRING(30),
      collate: 'utf8mb4_general_ci',
      defaultValue: null,
    },
    time: {
      type: DataTypes.STRING(255),
      collate: 'utf8mb4_general_ci',
      defaultValue: null,
    },
    communication_mode: {
      type: DataTypes.STRING(255),
      collate: 'utf8mb4_general_ci',
      defaultValue: null,
    },
    status: {
      type: DataTypes.TINYINT,
      defaultValue: 0,
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null
    },
  },
    {
      tableName: 'interior_design',
      collate: 'utf8mb4_unicode_ci',
      timestamps: false
    });
}