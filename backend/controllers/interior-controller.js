const { Op } = require("sequelize");
const { getPagingData } = require("../helpers/app-helper");
const CustomError = require("../helpers/errors/CustomError");
const { RESPONSE_CODES } = require("../helpers/errors/ResponseCodes");
const db = require("../models");

exports.createInterior = async (req, res, next) => {
  // console.log('req',req.body)
  try {
    let data = req.body
    let missingFields = []
    for (const [key, value] of Object.entries(data)) {
      if (!value) {
        missingFields.push(key);
      }
    }
    console.log("missingFields", missingFields)
    if (missingFields.length > 0) {
      throw new CustomError(RESPONSE_CODES.NOT_ACCEPTABLE, `Missing required fields: ${missingFields.join(', ')}`)
    }
    let existingRecord = await db.InteriorDesign.findOne({
      where: {
        email: data.email,
        phone: data.phone
      }
    })
    if (existingRecord) {
      throw new CustomError(RESPONSE_CODES.NOT_ACCEPTABLE, "Email or phone already exists.")
    }
    let { first_name, last_name, email, phone, gst_number, location, project_handle, time, communication_mode } = data;
    const created_at = new Date();
    let createdInterior = await db.InteriorDesign.create({
      first_name, last_name, email, phone, gst_number, location, project_handle, time, communication_mode, created_at
    })

    return res.status(200).json({ message: 'Record created successfully', data: createdInterior });
  } catch (error) {
    next(error)
  }
}

exports.getAllInterior = async (req, res, next) => {
  try {
    let { page, limit, status, search } = req.body;
    page = page ? parseInt(page) : 1;
    limit = limit ? parseInt(limit) : 20;

    const offset = (page - 1) * limit;
    let whereCondition = {}
    if (status) {
      whereCondition.status = status
    }
    if (search) {
      whereCondition = {
        ...whereCondition = {
          [Op.or]: [
            {
              first_name: { [Op.like]: `%${search}%` }
            },
            {
              last_name: { [Op.like]: `%${search}%` }
            },
            {
              email: { [Op.like]: `%${search}%` }
            }
          ]
        }
      }
    }
    let { rows, count } = await db.InteriorDesign.findAndCountAll({
      where: whereCondition,
      limit: limit,
      offset: offset
    })
    const pageData = getPagingData(count, limit, page);
    const response = {
      ...pageData,
      data: rows
    };
    res.status(200).json(response);
  } catch (error) {
    next(error)
  }
}

exports.updateInterior = async (req, res, next) => {
  try {
    const { id } = req.params;
    let updateData = req.body;
    // console.log('post',req.body, id)

    if (!updateData || Object.keys(updateData).length === 0) {
      throw new CustomError(400, 'No update data provided')
    }
    console.log("data for interior update", updateData)
    await db.InteriorDesign.update(updateData, {
      where: {
        id: id
      }
    })

    res.status(200).json({ message: "Interior updated." });
  } catch (error) {
    next(error)
  }
}

exports.deleteInterior = async (req, res, next) => {
  try {
    const id = req.params.id;
    await db.InteriorDesign.distroy({
      where: {
        id: id
      }
    })

    res.status(200).json({ message: "Interior deleted." });
  } catch (error) {
    next(error)
  }
}

exports.getInteriorCSV = async (req, res, next) => {
  try {
    let { status } = req.query;
    let whereCondition = {}
    if (status) {
      whereCondition.status = status
    }
    let response = await db.InteriorDesign.findAll({
      where: whereCondition
    })

    console.log("data", response);
    const csvData = await converter.json2csv(response);

    res.attachment('output.csv');
    res.setHeader('Content-Type', 'text/csv');

    res.status(200).send(csvData);

  } catch (error) {
    next(error)
  }
}