const { getPagingData } = require("../helpers/app-helper");
const CustomError = require("../helpers/errors/CustomError");
const { shopifyGraphqlClient } = require("../helpers/shopify_clients");
const db = require("../models");
const { getCustomer } = require("./customer-controller");
const fs = require('fs');

exports.createPost = async (req, res, next) => {
  try {
    let { domain, token } = req.storeData
    let { customer_id, image, link, title, description } = req.body;
    const customer = await getCustomer(customer_id, domain, token)
    // return customerId

    if (!link) link = ""

    const created_at = new Date();
    if (!image || !title || !description) throw new CustomError(422, 'Reequired information is incomplete or missing. Please provide a valid details.');

    const imageString = JSON.stringify(image);
    let postPayload = {
      customer_id: customer.id,
      image: imageString,
      link: link,
      title: title,
      description: description,
      created_at: created_at,
      status: '0'
    }

    let createdPost = await db.Posts.create(postPayload)

    return res.status(200).json({ message: 'Image uploaded successfully', data: createdPost });
  } catch (error) {
    next(error)
  }
}

// get all active posts
exports.getPost = async (req, res, next) => {
  try {
    let { page, limit } = req.query;
    page = page ? parseInt(page) : 1;
    limit = limit ? parseInt(limit) : 20;

    const offset = (page - 1) * limit;

    let { rows, count } = await db.Posts.findAndCountAll({
      where: {
        status: '1'
      },
      inculeds: {
        model: db.Customers,
        attributes: ['first_name', 'last_name', 'phone', 'email']
      },
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']],
    })
    const pageData = getPagingData(count, limit, page);
    const response = {
      ...pageData,
      data: rows
    };

    return res.status(200).json(response);
  } catch (error) {
    next(error)
  }
}

exports.uploadImage = async (req, res, next) => {
  try {
    // console.log('req.file',req.files)
    // console.log(req.body)
    const { domain, token } = req.storeData;
    const client = shopifyGraphqlClient({ shop: domain, accessToken: token });

    const fileUploadPromises = req.files.map(async (file) => {
      const url = await uploadFileToShopify(client, file.path);
      fs.unlinkSync(file.path); // Remove the file after upload
      return url;
    });
    const fileUrls = await Promise.allSettled(fileUploadPromises);
    let urls = [];
    for (const fileobj of fileUrls) {
      console.log("upload file url obj", fileobj.value)
      if (fileobj.status == "fulfilled") urls.push(fileobj.value)
    }
    res.status(200).json({ message: 'Image uploaded successfully', data: urls });
  } catch (error) {
    res.status(500).json({ message: 'Error uploading image', error: error });
  }
}

const uploadFileToShopify = async (client, filePath) => {
  const mutation = `
    mutation fileCreate($files: [FileCreateInput!]!) {
      fileCreate(files: $files) {
        files {
          alt
          createdAt
          fileStatus
          preview {
            image {
              originalSrc
            }
          }
        }
        userErrors {
          field
          message
        }
      }
    }
  `;

  const fileContent = fs.readFileSync(filePath, 'base64');

  const variables = {
    files: [
      {
        alt: "Uploaded file",
        contentType: "IMAGE",
        originalSource: `data:image/jpeg;base64,${fileContent}`
      }
    ]
  };

  try {
    const response = await client.query({
      data: {
        query: mutation,
        variables: variables,
      },
    });

    if (response.body.errors) {
      console.error("Error uploading file:", response.body.errors);
      return null;
    }

    return response.body.data.fileCreate.files[0].preview.image.originalSrc;
  } catch (error) {
    console.error("Error uploading file:", error);
    return null;
  }
};

exports.getAllPost = async (req, res, next) => {
  try {
    let { page, limit, status } = req.body;
    page = page ? parseInt(page) : 1;
    limit = limit ? parseInt(limit) : 20;

    const offset = (page - 1) * limit;
    let whereCondition = {}
    if (status) {
      whereCondition.status = status
    }
    let { rows, count } = await db.Posts.findAndCountAll({
      where: whereCondition,
      include: {
        model: db.Customers,
        attributes: ['first_name', 'last_name', 'phone', 'email']
      },
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']],
    })
    const pageData = getPagingData(count, limit, page);
    const response = {
      ...pageData,
      data: rows
    };

    return res.status(200).json(response);
  } catch (error) {
    next(error)
  }
}

exports.updatePost = async (req, res, next) => {
  try {
    const { id } = req.params;
    let updateData = req.body
    console.log("data to update post", updateData)
    if (!updateData || Object.keys(updateData).length === 0) throw new CustomError(400, 'No update data provided');
    let updatedPosts = db.Posts.update(updateData, {
      where: {
        id: id
      }
    })

    res.status(200).json({ message: "Updated succesfully." });
  } catch (error) {
    next(error)
  }
}

exports.deletePost = async (req, res) => {
  try {
    const id = req.params.id;
    const data = await db.Posts.distory({
      where: {
        id: id
      }
    })
    res.status(200).json({ message: "Post deleted." });
  } catch (error) {
    console.log(error)
    next(new CustomError(500, "Error to delete post."))
  }
}

exports.getPostsCSV = async (req, res) => {
  try {
    let { status } = req.body;
    let whereCondition = {}
    if (status) {
      whereCondition.status = status
    }
    let postData = await db.Posts.findAll({
      where: whereCondition,
      inculeds: {
        model: db.Customers,
        attributes: ['first_name', 'last_name', 'phone', 'email']
      },
      rows: true
    })

    const csvData = await converter.json2csv(postData);

    res.attachment('output.csv');
    res.setHeader('Content-Type', 'text/csv');

    return res.status(200).send(csvData);

  } catch (error) {
    console.log("error", error)
    next(new CustomError(500, 'Something went wrong'));
  }
}