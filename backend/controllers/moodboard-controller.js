const { Op } = require("sequelize");
const CustomError = require("../helpers/errors/CustomError");
const db = require("../models");
const { getCustomer } = require("./customer-controller");
const { shopifyStorefrontClient } = require("../helpers/shopify_clients");
const { getCustomerColors, getColors } = require("./colors-controller");
const { getPagingData } = require("../helpers/app-helper");
let moment = require('moment');
const converter = require('json-2-csv');

exports.createMoodboard = async (req, res, next) => {
  try {
    let { domain, token, storefront_token } = req.storeData
    let { customer_id, products, selected_products, selected_colors, title, description } = req.body;
    if (!customer_id || customer_id == 'undefined' || !title) throw new CustomError(422, 'Required information is missing.');
    const customer = await getCustomer(customer_id, domain, token)
    if (!customer) throw new CustomError(422, 'Customer not found.');

    if (!Array.isArray(products)) products = []
    if (!Array.isArray(selected_products)) selected_products = []
    if (!Array.isArray(selected_colors)) selected_colors = []

    if (selected_colors.length > 0) {
      let verifyColorsIds = await db.Colors.findAll({
        where: {
          id: {
            [Op.in]: selected_colors
          }
        }
      })
      selected_colors = verifyColorsIds.map((item) => (item.id))
      if (selected_colors.length < 4) {
        let admin_colors = await db.Colors.findAll({
          where: {
            customer_id: {
              [Op.is]: null
            }
          },
          limit: (4 - selected_colors.length)
        })
        selected_colors = selected_colors.concat(admin_colors.map((item) => (item.id)))
      }
    }

    selected_products = selected_products.map((item) => {
      let arrId = item.split("/")
      return arrId[arrId.length - 1]
    })
    products = products.map((item) => {
      let arrId = item.split("/")
      return arrId[arrId.length - 1]
    })

    let createPayload = {
      customer_id: customer.id,
      products: products,
      selected_products: selected_products,
      selected_colors: selected_colors,
      title: title,
      description: description || undefined,
    }

    let createdMoodboard = await db.Moodboards.create(createPayload)

    return res.status(200).json({ message: 'Moodboard created.', data: createdMoodboard });
  } catch (error) {
    next(error)
  }
}


exports.getMoodboards = async (req, res, next) => {
  try {
    let { domain, token } = req.storeData
    let { customer_id } = req.params;
    if (!customer_id || customer_id == 'undefined') throw new CustomError(422, 'Required information is missing.');
    const customer = await getCustomer(customer_id, domain, token)

    let moodboards = await db.Moodboards.findAll({
      where: {
        customer_id: customer.id
      }
    })
    if (moodboards) moodboards = JSON.parse(JSON.stringify(moodboards));
    for (let moodboard of moodboards) {
      let colors = await getColors(moodboard.selected_colors)
      moodboard.selected_colors = colors
      moodboard.selected_products = moodboard?.selected_products?.map((ids) => (`gid://shopify/Product/${ids}`)) || []

    }

    let customer_color_codes = await getCustomerColors(customer_id)
    customer.customer_color_codes = customer_color_codes

    return res.status(200).json({ customer, moodboards });
  } catch (error) {
    next(error)
  }
}

exports.getMoodboard = async (req, res, next) => {
  try {
    let { domain, token, storefront_token } = req.storeData
    let { id } = req.params;

    if (!id) throw new CustomError(422, 'Required information is missing.');

    let moodboard = await this.getMoodboardDataFromID(id)
    moodboard = await this.getModifiyMoodboardData(moodboard, { domain, token, storefront_token })
    return res.status(200).json({ data: moodboard });
  } catch (error) {
    next(error)
  }
}

const getProductsFromShopify = async (product_ids, domain, token, storefront_token) => {
  try {
    // parse error
    // product_ids = JSON.parse(product_ids);
    let schema = {
      query: `query getProducts($first: Int, $query: String) {
                products(first: $first, query: $query) {
                  edges {
                    node {
                      id
                      title
                      featuredImage {
                        id
                        src
                        altText
                      }
                      handle
                      metafield(key: "modboard_image",namespace: "custom") {
                        id
                        value
                        type
                        namespace
                        reference {
                          ... on MediaImage {
                            id
                            image {
                              src
                              id
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            `,
      variables: {
        query: `id:${product_ids.join(" OR id:")}`,
        first: product_ids.length
      }
    }
    let shopify = shopifyStorefrontClient({ shop: domain, storefrontToken: storefront_token, token })
    let products = await shopify.query({ data: schema })

    return products?.body?.data?.products?.edges

  } catch (error) {
    console.log(error)
    throw new CustomError(500, "Shopify Error to get products.")
  }
}

exports.updateMoodboard = async (req, res, next) => {
  try {
    let id = req.params.id
    if (!id) throw new CustomError(422, 'Required information is missing.');
    let moodboardData = await db.Moodboards.findOne({
      where: {
        id: id
      }
    })
    if (!moodboardData) throw new CustomError(422, 'Moodboard not found.');
    let { products, selected_products, selected_colors, title, description } = req.body;

    if (!Array.isArray(products)) products = []
    if (!Array.isArray(selected_products)) selected_products = []
    if (!Array.isArray(selected_colors)) selected_colors = []

    if (selected_colors.length > 0) {
      let verifyColorsIds = await db.Colors.findAll({
        where: {
          id: {
            [Op.in]: selected_colors
          }
        }
      })
      selected_colors = verifyColorsIds.map((item) => (item.id))
    }

    selected_products = selected_products.map((item) => {
      let arrId = item.split("/")
      return arrId[arrId.length - 1]
    })
    products = products.map((item) => {
      let arrId = item.split("/")
      return arrId[arrId.length - 1]
    })

    let postPayload = {
      products: products,
      selected_products: selected_products,
      selected_colors: selected_colors,
      title: title || undefined,
      description: description || undefined,
    }

    await db.Moodboards.update(postPayload, {
      where: {
        id: id
      }
    })

    return res.status(200).json({ message: 'Moodboard updated.' });
  } catch (error) {
    next(error)
  }
}

exports.addMoodboardProduct = async (req, res, next) => {
  try {
    let { domain, token, storefront_token } = req.storeData
    let id = req.params.id
    let { product_id } = req.body
    if (!id || !product_id) throw new CustomError(422, 'Required information is missing.');
    let moodboard = await this.getMoodboardDataFromID(id, { domain, storefront_token, token }, { addProducts: product_id })

    if (!Array.isArray(moodboard.products)) moodboard.products = []
    if (!moodboard.products.includes(product_id)) {
      moodboard.products.push(product_id)
    }

    let postPayload = {
      products: moodboard.products
    }

    await db.Moodboards.update(postPayload, {
      where: {
        id: id
      }
    })

    moodboard = await this.getModifiyMoodboardData(moodboard, { domain, storefront_token, token })
    return res.status(200).json({ message: 'Moodboard updated.', data: moodboard });
  } catch (error) {
    next(error)
  }
}
exports.removeMoodboardProduct = async (req, res, next) => {
  try {
    let { domain, token, storefront_token } = req.storeData
    let id = req.params.id
    let { product_id } = req.body
    if (!id || !product_id) throw new CustomError(422, 'Required information is missing.');
    let moodboard = await this.getMoodboardDataFromID(id)

    if (!Array.isArray(moodboard.products)) moodboard.products = []
    let products = moodboard.products
    let removeIndex = products.findIndex((item)=>(item == product_id))
    if (removeIndex > -1) products.splice(removeIndex, 1)

    let postPayload = {
      products: products
    }

    await db.Moodboards.update(postPayload, {
      where: {
        id: id
      }
    })
    moodboard.products = products
    moodboard = await this.getModifiyMoodboardData(moodboard, { domain, storefront_token, token })

    return res.status(200).json({ message: 'Moodboard updated.', data: moodboard });
  } catch (error) {
    next(error)
  }
}


exports.getAllMoodboards = async (req, res, next) => {
  try {
    let { domain, token, storefront_token } = req.storeData
    let { page, limit, search } = req.body;
    page = parseInt(page) || 1
    limit = parseInt(limit) || 20

    let dates = search.dates;
    let serchData = search.search;

    if (!dates) dates = {}
    if (!dates?.start) {
      dates.start = moment().startOf('M').toDate()
    } else {
      dates.start = moment(dates.start).toDate();
    }
    if (!dates?.end) dates.end = moment().endOf('D').add(1, 'days').toDate();
    else {
      dates.end = moment(dates.end).add(1, 'days').toDate();
    }

    const offset = (page - 1) * limit;
    let whereCondition = {
      created_at: {
        [Op.between]: [dates.start, dates.end]
      }
    };

    if (serchData) {
      whereCondition.title = {
        [Op.like]: `%${serchData}%`
      };
    }

    let { rows, count } = await db.Moodboards.findAndCountAll({
      where: whereCondition,
      include: {
        model: db.Customers,
        attributes: ['first_name', 'last_name', 'phone', 'email']
      },
      limit,
      offset,
      distinct: true,
      order: [['created_at', 'DESC']],
    })

    if (rows.length > 0) rows = JSON.parse(JSON.stringify(rows))
    const pageData = getPagingData(count, limit, page);
    let moodboards = []

    for (const item of rows) {
      let moodboard = await this.getModifiyMoodboardData(item, { domain, token, storefront_token, selected_colors: true })
      moodboards.push(moodboard)
    }
    const response = {
      ...pageData,
      data: moodboards
    };

    return res.status(200).json(response);
  } catch (error) {
    next(error)
  }
};

exports.exportMoodboards = async (req, res, next) => {
  try {
    let { dates, exportAll } = req.body;

    if (!dates) dates = {};
    if (!dates?.start) {
      dates.start = moment().startOf('M').toDate();
    } else {
      dates.start = moment(dates.start).toDate();
    }
    if (!dates?.end) dates.end = moment().endOf('D').add(1, 'days').toDate();
    else {
      dates.end = moment(dates.end).add(1, 'days').toDate();
    }


    // Raw SQL query to fetch moodboards with related customer and colors data
    const query = `
  SELECT 
    m.id,
    m.title,
    m.description,
    m.products,
    m.selected_products,
    m.created_at,
    c.first_name,
    c.last_name,
    c.phone,
    c.email,
    GROUP_CONCAT(cl.name) AS selected_colors
  FROM 
    moodboards m
  JOIN 
    customers c ON c.id = m.customer_id
  LEFT JOIN 
    colors cl ON JSON_CONTAINS(m.selected_colors, JSON_ARRAY(cl.id)) > 0
  ${!exportAll ? ' WHERE m.created_at BETWEEN ? AND ?' : '' }  
  GROUP BY 
    m.id, c.id
  ORDER BY 
    m.created_at DESC
  ;
`;
    const result = await db.sequelize.query(query,
      {
         replacements: !exportAll ? [dates.start, dates.end] : [],
          type: db.sequelize.QueryTypes.SELECT }
    );

    const rows= result;
    if (rows.length > 0) {
      let exportData = rows.map(element => ({
        Title: element.title,
        Name: element.first_name,
        Email: element.email,
        Phone: element.phone ? `${element.phone.toString()}` : "",
        Description: element.description || "",
        Products: element.products || "",
        // Selected_products: element.selected_products || "",
        Selected_Colors: element.selected_colors || "",
      }));

      const csvString = converter.json2csv(exportData);

      res.writeHead(200, {
        'Content-Type': 'text/csv',
        'Content-disposition': 'attachment; filename=Review_Data.csv'
      });

      res.status(200).end(csvString);
    } else {
      res.status(404).send('No moodboards found for the specified conditions.');
    }
  } catch (error) {
    next(error);
  }
};




exports.getMoodboardProducts = async (req, res, next) => {
  try {

    return res.status(200).json({});
  } catch (error) {
    next(error)
  }
}

exports.getMoodboardDataFromID = async (id) => {
  try {
    let moodboard = await db.Moodboards.findOne({
      where: {
        id: id
      },
      include: {
        model: db.Customers,
        attributes: ['first_name', 'last_name', 'phone', 'email']
      }
    })

    if (!moodboard) throw new CustomError(400, "Moodboard not found.");
    moodboard = JSON.parse(JSON.stringify(moodboard))

    return moodboard
  } catch (error) {
    throw error
  }
}

exports.getModifiyMoodboardData = async (moodboard, { domain, token, storefront_token, selected_colors }) => {
  try {
    if (domain && token && storefront_token) {
      //get shopify product
      let products = await getProductsFromShopify(moodboard.products, domain, token, storefront_token)
      let productsArr = []
      for (let obj of products) {
        if (obj?.node?.metafield) {
          let modboard_image = obj?.node?.metafield?.reference?.image?.src || null
          if (modboard_image && obj?.node?.featuredImage) {
            obj.node.featuredImage.src = modboard_image
          }
          delete obj.node.metafield
        }
        productsArr.push(obj)
      }
      moodboard.products = productsArr
    }
    // parse error
    // moodboard.selected_products = JSON.parse(moodboard?.selected_products);
    moodboard.selected_products = moodboard?.selected_products?.map((ids) => (`gid://shopify/Product/${ids}`)) || []
    if (selected_colors) {
      moodboard.selected_colors = await getColors(moodboard.selected_colors)
    } else {
      // get colors
      let colors = await getCustomerColors(moodboard.customer_id)
      moodboard.colors = colors
    }
    return moodboard
  } catch (error) {
    throw error
  }
}
