const CustomError = require("../helpers/errors/CustomError")
const { shopifyRestClient } = require("../helpers/shopify_clients")
const db = require("../models")


exports.getCustomer = async (customer_id, domain, token) => {
  try {
    let customer = await db.Customers.findOne({
      where: {
        customer_id: customer_id
      }
    })
    // create customer to database
    if (!customer) {
      // get customer from shopify 
      const shopify = shopifyRestClient({ shop: domain, accessToken: token })
      let response = await shopify.get({ path: `customers/${customer_id}` })
      let shopifyCustomer = response?.body?.customer

      const customerData = {
        first_name: shopifyCustomer?.first_name || shopifyCustomer?.default_address?.first_name,
        last_name: shopifyCustomer?.last_name || shopifyCustomer?.default_address?.last_name,
        phone: shopifyCustomer?.phone || shopifyCustomer?.default_address?.phone,
        email: shopifyCustomer?.email || shopifyCustomer?.default_address?.email,
        customer_id: shopifyCustomer?.id || shopifyCustomer?.default_address?.id,
        created_at: new Date()
      }

      customer = await db.Customers.create(customerData)
    }

    return customer
  } catch (error) {
    console.log("Shopify error to find customer.", error)
    throw new CustomError(400, "Customer not found.")
  }
}
