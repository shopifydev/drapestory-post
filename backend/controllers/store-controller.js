const { where, Op } = require("sequelize")
const db = require("../models")

exports.getStoreDetails = (req, res, next) => {
  try {
    let store = req.storeData
    delete store.token

    return res.status(200).send({ data: store })
  } catch (error) {
    next(error)
  }
}

exports.getDashboardDetails = async (req, res, next) => {
  try {
    let store = req.storeData
    let postsCount = await db.Posts.count()
    let moodboardsCount = await db.Moodboards.count()
    let interiorCount = await db.InteriorDesign.count()
    let colorsCount = await db.Colors.count({
      where: {
        customer_id: {
          [Op.is]: null
        }
      }
    })

    return res.status(200).send({ data: { posts: postsCount, moodboards: moodboardsCount, interiors: interiorCount, colors: colorsCount } })
  } catch (error) {
    next(error)
  }
}