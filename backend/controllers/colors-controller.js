const { Op } = require("sequelize")
const db = require("../models")
const CustomError = require("../helpers/errors/CustomError")
const { getCustomer } = require("./customer-controller")

exports.getCustomerColors = async (customer_id) => {
  try {
    let colors = await db.Colors.findAll({
      where: {
        [Op.or]: [
          {
            customer_id: customer_id,
          }, {
            customer_id: {
              [Op.is]: null
            }
          }
        ]
      },
      attributes: ['id', 'code', 'name', 'image']
    })
    if (colors) colors = JSON.parse(JSON.stringify(colors));

    return colors
  } catch (error) {
    console.log(error)
    throw new CustomError(422, "Error to get colors.")

  }
}
exports.getColors = async (ids) => {
  // parse error
  // ids = JSON.parse(ids);
  try {
    let listOfColors = await db.Colors.findAll({
      where: {
        id: {
          [Op.in]: ids
        }
      },
      attributes: ['id', 'code', 'name', 'image']
    })
    if (listOfColors) listOfColors = JSON.parse(JSON.stringify(listOfColors))
    // let arrayOfColors = colors.map((item) => (item.code || item.image))

    return listOfColors
  } catch (error) {
    console.log(error)
    throw new CustomError(422, "Error to get colors.")
  }
}

exports.customerColors = async (req, res, next) => {
  try {
    let colors = await this.getCustomerColors(req.params.customer_id)

    return res.status(200).send({ data: colors })
  } catch (error) {
    console.log(error)
    next(error)
  }
}

exports.createColor = async (req, res, next) => {
  try {
    let { domain, token } = req.storeData
    let { customer_id, name, code, image } = req.body

    if (!customer_id || customer_id == 'undefined' || !code || code == 'undefined') throw new CustomError(422, 'Reequired information is incomplete or missing. Please provide a valid details.');
    const customer = await getCustomer(customer_id, domain, token)

    let defaultColorCode = await db.Colors.findOne({
      where: {
        code: code,
        [Op.or]: [{
          customer_id: customer.id,
        }, {
          customer_id: {
            [Op.is]: null
          }
        }]
      }
    })

    if (defaultColorCode) throw CustomError(422, "Color code already exits.")
    let createdColor = await db.Colors.create({ customer_id: customer.id, name, code, image })

    res.status(200).send({ message: "Color created.", data: createdColor })
  } catch (error) {
    next(error)
  }
}

exports.getAdminColors = async (req, res, next) => {
  try {
    let listOfColors = await db.Colors.findAll({
      where: {
        customer_id: {
          [Op.is]: null
        }
      },
      attributes: ['id', 'code', 'name', 'image']
    })
    if (listOfColors) listOfColors = JSON.parse(JSON.stringify(listOfColors))

    res.status(200).send({ data: listOfColors })
  } catch (error) {
    next(error)
  }
}

exports.createAdminColor = async (req, res, next) => {
  try {
    let { name, code } = req.body

    if (!code) throw new CustomError(422, 'Color code not found');

    let defaultColorCode = await db.Colors.findOne({
      where: {
        code: code,
        customer_id: {
          [Op.is]: null
        }
      }
    })

    if (defaultColorCode) throw new CustomError(422, "Color code already exits.")
    let createdColor = await db.Colors.create({ name, code })

    res.status(200).send({ message: "Color created.", data: createdColor })
  } catch (error) {
    next(error)
  }
};

exports.deleteMultipleColors = async (req, res, next) => {
  try {
    const colorIds = req.body; 

    if (!colorIds || !Array.isArray(colorIds) || colorIds.length === 0) {
      throw new CustomError(422, "No color IDs provided.");
    }

    // Find and delete the colors by their IDs
    const deletedColors = await db.Colors.destroy({
      where: {
        id: {
          [Op.in]: colorIds, 
        },
      },
    });

    if (deletedColors === 0) {
      throw new CustomError(404, "No matching colors found to delete.");
    }

    res.status(200).send({ message: "Colors deleted successfully." });
  } catch (error) {
    next(error);
  }
};
