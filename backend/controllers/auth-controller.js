
const axios = require("axios");
const CONFIG = require("../config");
const CustomError = require("../helpers/errors/CustomError");
const { RESPONSE_CODES, RESPONSE_MESSAGES } = require("../helpers/errors/ResponseCodes");
const { shopifyRestClient } = require("../helpers/shopify_clients");
const db = require("../models");
const crypto = require('node:crypto');
const { getCurrencySymbols } = require("../helpers/app-helper");
const { webhookCreateObject } = require("../helpers/shopify_clients/webhook");

exports.login = (req, res, next) => {
  let shop = req.query.shop || req.body.shop;
  if (!shop) {
    return next(new CustomError(RESPONSE_CODES.BAD_REQUEST, "Required parameters missing"))
  }
  let authUrl = "https://" + shop + "/admin/oauth/authorize?client_id=" + CONFIG.SHOPIFY.API_KEY + "&scope=" + CONFIG.SHOPIFY.SCOPES + "&redirect_uri=" + CONFIG.SHOPIFY.REDIRECT_URL
  // authUrl = 
  // https://admin.shopify.com/store/spacesdrapestory/oauth/install_custom_app?client_id=f43c3ab2176c0759d39f376d243dc312&no_redirect=true&signature=eyJleHBpcmVzX2F0IjoxNzI0NzU3NjUyLCJwZXJtYW5lbnRfZG9tYWluIjoic3BhY2VzZHJhcGVzdG9yeS5teXNob3BpZnkuY29tIiwiY2xpZW50X2lkIjoiZjQzYzNhYjIxNzZjMDc1OWQzOWYzNzZkMjQzZGMzMTIiLCJwdXJwb3NlIjoiY3VzdG9tX2FwcCIsIm1lcmNoYW50X29yZ2FuaXphdGlvbl9pZCI6NzA3MDM1NTZ9--45ea435772d3f1053ec46ceaf2c23fb5e7cb70ed&scope=read_products,unauthenticated_read_product_listings
  console.log("authUrl",authUrl)
  res.redirect(authUrl);
};

exports.generateToken = async (req, res) => {
  const { shop, hmac, code, host } = req.query;

  try {
    if (shop && hmac && code && host) {

      const map = Object.assign({}, req.query);
      delete map['hmac'];
      const urlSearchParams = new URLSearchParams(map);
      const message = urlSearchParams.toString();
      const generatedHash = crypto
        .createHmac('sha256', CONFIG.SHOPIFY.SECRET_KEY)
        .update(message)
        .digest('hex');

      if (generatedHash !== hmac) {
        throw new CustomError(400, 'Invalid Credential')
      }
      console.log("pass 111")
      // Generate access token by code
      let accessTokenResponse = await axios.post("https://" + shop + "/admin/oauth/access_token", {
        client_id: CONFIG.SHOPIFY.API_KEY,
        client_secret: CONFIG.SHOPIFY.SECRET_KEY,
        code: code
      }, {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      console.log("accessTokenResponse",accessTokenResponse)
      if (!accessTokenResponse.data) {
        throw new CustomError(RESPONSE_CODES.UNAUTHORIZED, "Token Invalid");
      }

      const accessToken = accessTokenResponse.data.access_token

      // Get Shop data from Shopify
      const shopify = await shopifyRestClient({ shop: shop, accessToken: accessToken })
      let response = await shopify.get({ path: 'shop' })
      if (!response) {
        throw new CustomError(RESPONSE_CODES.UNAUTHORIZED, "Unauthorize shop");
      }
      let shopData = response.body.shop;
      if (!shopData.storefront_token) {
        let storefront_access_tokens = await shopify.post({ path: 'storefront_access_tokens', data: { storefront_access_token: { title: "Drapstory-app-product" } } })
        if (storefront_access_tokens) {
          shopData.storefront_token = storefront_access_tokens?.body?.storefront_access_token?.access_token
        }
      }
      // Upsert Shop data into database
      let shopResp = await insertShopData(shopData, accessToken, shopData.storefront_token);
      // create webhooks

      if (process.env.NODE_ENV != "local") {
         this.registerWebhook(shop, accessToken)
      }

      // redirect path 
      let pathname = '/admin/dashboard';

      return res.redirect(`${pathname}` + '?' + 'host=' + host + '&shop=' + shop);
    } else {
      throw new CustomError(RESPONSE_CODES.NOT_FOUND, RESPONSE_MESSAGES.MISSING_BODY);
    }
  } catch (error) {
    console.log("Error generateToken--->", error);
    let code = error.statusCode ? error.statusCode : 500
    res.status(code).send({ statusCode: code, message: error.message })
  }
};

const insertShopData = (data, token, storefront_token) => {
  return new Promise(async (resolve, reject) => {
    try {
      const shopDataObj = {
        store_id: data.id,
        username: data.name,
        email: data.email,
        domain: data.myshopify_domain,
        shop_created_at: data.created_at,
        shop_owner: data.shop_owner,
        token: token,
        iana_timezone: data.iana_timezone,
        guid: data.id,
        status: data.financy,
        is_deleted: '0',
        currency: data.currency,
        tax_included: data.taxes_included,
        currency_format: getCurrencySymbols(data.currency)
      }
      if (storefront_token) shopDataObj.storefront_token = storefront_token;
      let shopResponse = await db.Store.findOne({ where: { domain: data.myshopify_domain } })
      if (shopResponse) {
        await db.Store.update(shopDataObj, { where: { domain: data.myshopify_domain } })
        resolve({ id: shopResponse.id })
      } else {
        let storeResponse = await db.Store.create(shopDataObj)
        resolve(storeResponse)
      }
    } catch (error) {
      console.log("insertShopData ", error)
      reject(false)
    }
  })
};

exports.registerWebhook = async (domain, token) => {
  const shopify = await shopifyRestClient({ shop: domain, accessToken: token })
  const webhooks = await shopify.get({ path: 'webhooks' });
  var webhookList = webhooks.body.webhooks
  var webhookData = webhookCreateObject();
  for (let webhook of webhookData) {
    var isCreateWebhook = webhookList.filter(x => x.topic === webhook.topic && x.address === webhook.address);
    if (isCreateWebhook.length > 0) {
      continue;
    } else {
      shopify.post({ path: 'webhooks', data: { webhook } }).then((response, error) => {
        console.log("Created");
      }).catch(err => {
        console.log("registerWebhook", err)
      })
    }
  }
}

