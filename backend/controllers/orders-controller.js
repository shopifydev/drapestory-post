const { Op } = require("sequelize")
const db = require("../models")
const CustomError = require("../helpers/errors/CustomError");
const { getPagingData } = require("../helpers/app-helper");
let moment = require('moment');


exports.getAllOrders = async (req, res, next) => {
    try {
        let { page, limit, search } = req.body;
        page = parseInt(page) || 1
        limit = parseInt(limit) || 20

        let dates = search.dates;
        let serchData = search.search;

        if (!dates) dates = {}
        if (!dates?.start) {
            dates.start = moment().startOf('M').toDate()
        } else {
            dates.start = moment(dates.start).toDate();
        }
        if (!dates?.end) dates.end = moment().endOf('D').add(1, 'days').toDate();
        else {
            dates.end = moment(dates.end).add(1, 'days').toDate();
        }

        const offset = (page - 1) * limit;
        // let whereCondition = {
        //     created_at: {
        //         [Op.between]: [dates.start, dates.end]
        //     }
        // };
        let whereCondition = {};

        if (serchData) {
            whereCondition = {
                [Op.or]: [
                    {
                        order_name: {
                            [Op.like]: `%${serchData}%`  // Fixed the template literal here
                        }
                    },
                    {
                        customer_name: {
                            [Op.like]: `%${serchData}%`
                        }
                    },
                    {
                        customer_email: {
                            [Op.like]: `%${serchData}%`
                        }
                    },
                    {
                        amount: {
                            [Op.like]: `%${serchData}%`
                        }
                    }
                ]
            };
        }

        console.log(whereCondition, "whereCondition");
        let { rows, count } = await db.order.findAndCountAll({
            where: whereCondition,
            // include: {
            //   model: db.Customers,
            //   attributes: ['first_name', 'last_name', 'phone', 'email']
            // },
            limit,
            offset,
            distinct: true,
            order: [['created_at', 'DESC']],
        })

        if (rows.length > 0) rows = JSON.parse(JSON.stringify(rows))
        const pageData = getPagingData(count, limit, page);

        const response = {
            ...pageData,
            data: rows
        };

        return res.status(200).json(response);
    } catch (error) {
        next(error)
    }
};