const { QueryTypes } = require("sequelize");
const db = require("../models");
const CONFIG = require("../config");

exports.shopifyWebhookHandler = async (req, res) => {
    try {
        let domain, webhookTopic, webhookData;
        domain = req.headers["x-shopify-shop-domain"];
        webhookTopic = req.headers["x-shopify-topic"];
        webhookData = req.body;

        if (!domain) {
            throw new Error("Shop domain not provided");
        }

        const result = await db.sequelize.query(
            `SELECT * FROM stores WHERE domain = :domain`,
            { replacements: { domain }, type: QueryTypes.SELECT }
        );

        if (result && webhookData) {
            let store = result[0];

            console.log("topic", webhookTopic);
            switch (webhookTopic) {
                case "orders/create":
                case "orders/updated":
                    await createAndUpdateOrder(webhookData);
                    break;
                default:
                    console.log("Unhandled webhook topic:", webhookTopic);
            }
        }

        res.status(200).send();
    } catch (error) {
        console.log("shopify_webhook_handler error:", error);
        res.status(200).send();
    }
};

const createAndUpdateOrder = async (order) => {
    try {
        if (!order) {
            throw new Error("Order or store details missing.");
        }

        if (!order.id || !order.name) {
            throw new Error("Missing required order fields.");
        }
        const tagToFind = '_moodboard';
        const tagExists = order.line_items
        ? order.line_items.some(item => {
            return item.properties && item.properties.some(property => {
              return property.name === tagToFind;
            });
          })
        : false;

        if (tagExists) {
            const existingOrder = await db.order.findOne({ where: { order_id: order.id } });
            if (existingOrder) {
                console.log("Order already exists. Skipping insertion.");
                return existingOrder;
            }
            let customer_name = order.customer ? order.customer.first_name + order.customer.last_name || "" : null;

            const orderData = {
                order_id: order.id,
                order_name: order.name,
                amount: parseFloat(order.total_price) || 0,
                customer_email: order.customer ? order.customer.email : null,
                customer_name: customer_name,
                customer_id: order.customer ? order.customer.id : null,
            };

            const newOrder = await db.order.create(orderData);
            console.log("New order created:", newOrder);
            return newOrder;
        } else {
            console.log("Orders are not coming from MoodBoard.");
        }
    } catch (error) {
        console.error("Error in createAndUpdateOrder:", error);
        throw new Error(`Error processing order ${order?.name || 'unknown order'}`);
    }
};
