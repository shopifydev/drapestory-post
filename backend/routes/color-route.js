
module.exports = app => {
  const colorController = require("../controllers/colors-controller.js");

  var router = require("express").Router();

  // using server side
  router.get('/colors', colorController.getAdminColors)
  router.post('/create-color', colorController.createAdminColor)
  router.delete('/delete-colors', colorController.deleteMultipleColors)

  app.use('/api', router);
};