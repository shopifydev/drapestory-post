module.exports = app => {
	const authController = require("../controllers/auth-controller.js");

	var router = require("express").Router();

	// Login API Routes
	router.get("/shoplogin", authController.login);
	router.post("/login", authController.login);
	router.get("/auth/callback", authController.generateToken);

	app.use('/', router);
};