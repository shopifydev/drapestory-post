
module.exports = app => {
	const postController = require("../controllers/post-controller.js");

  var router = require("express").Router();

  // using server side
  router.post('/get-all-posts', postController.getAllPost);
  router.post('/update-post/:id', postController.updatePost);
  router.delete('/delete-post/:id', postController.deletePost);
  router.get('/get-posts-csv', postController.getPostsCSV);

  app.use('/api', router);
};