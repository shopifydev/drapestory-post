
module.exports = app => {
  const storeController = require("../controllers/store-controller.js");

  var router = require("express").Router();

  // using server side
  router.get('/store-details', storeController.getStoreDetails);
  router.get('/dashboard-details', storeController.getDashboardDetails);

  app.use('/api', router);
};