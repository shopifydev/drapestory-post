
module.exports = app => {
    const orderController = require("../controllers/orders-controller");
  
    const router = require("express").Router();
  
    // using server side
    router.post('/orders', orderController.getAllOrders)
    app.use('/api', router);
  };