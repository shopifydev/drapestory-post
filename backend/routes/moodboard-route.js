
module.exports = app => {
  const moodboardController = require("../controllers/moodboard-controller.js");

  const router = require("express").Router();

  // using server side
  router.post('/all-moodboards', moodboardController.getAllMoodboards)
  router.post('/export-moodboard', moodboardController.exportMoodboards)
  router.get('/moodboard/products', moodboardController.getMoodboardProducts)

  app.use('/api', router);
};