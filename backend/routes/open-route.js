
module.exports = app => {
	const { uploadMiddleware } = require("../middleware/file-upload.js");
	const moodboardController = require("../controllers/moodboard-controller.js");
	const postController = require("../controllers/post-controller.js");
	const interiorControllers = require("../controllers/interior-controller.js");
	const colorControllers = require("../controllers/colors-controller.js");

	var router = require("express").Router();

	// using frontend side
	router.post("/create-post", postController.createPost);
	router.get('/get-posts', postController.getPost);
	router.post('/upload-image', uploadMiddleware, postController.uploadImage);
	router.post('/create-interior', interiorControllers.createInterior);
	router.post('/create-moodboard', moodboardController.createMoodboard)
	router.get('/moodboards/:customer_id', moodboardController.getMoodboards)
	router.get('/moodboard/:id', moodboardController.getMoodboard)
	router.post('/update-moodboard/:id', moodboardController.updateMoodboard)
	router.post('/add-product/:id', moodboardController.addMoodboardProduct)
	router.post('/remove-product/:id', moodboardController.removeMoodboardProduct)
	router.post('/create-color', colorControllers.createColor)
	router.get('/get-color/:customer_id', colorControllers.customerColors)
	router.get('/get-colors', colorControllers.getColors)


	app.use('/frontend', router);
};