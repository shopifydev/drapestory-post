
module.exports = app => {
	const interiorController = require("../controllers/interior-controller.js");

  var router = require("express").Router();

  // using server side
  router.post('/get-all-interior', interiorController.getAllInterior);
  router.post('/update-interior/:id',  interiorController.updateInterior);
  router.delete('/delete-interior/:id',  interiorController.deleteInterior);
  router.get('/get-interior-csv',  interiorController.getInteriorCSV);
  
  app.use('/api', router);
};