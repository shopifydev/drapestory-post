const mode = process.env?.NODE_ENV?.trim();
const users = [{ id: 1, email: 'info@drapestory.in', password: 'drapestory@987' }]
module.exports = {
    storeUrl: process.env.storeUrl,
    shopifyAPIKey: process.env.shopifyAPIKey,
    shopifySecretKey: process.env.shopifySecretKey,
    shopifyUrl: process.env.shopifyUrl,
    shopifyAccessToken: process.env.shopifyAccessToken,    
    allowedDomains : (mode == 'local' || mode == 'development' ) ? process.env.LOCAL_allowedDomains : process.env.allowedDomains,
    users : users,
    mode: mode,
    secretKey: process.env.SECRETKEY,
    db: (mode == 'local' || mode == 'development' ) ? {
        host: process.env.LOCAL_HOST,
        database: process.env.LOCAL_DATABASE,
        port: process.env.LOCAL_DBPORT,
        user: process.env.LOCAL_USER,
    }:
    {
        host: process.env.DBHOST,
        database: process.env.DBDATABASE,
        port: process.env.DBPORT,
        user: process.env.DBUSER,
        password: process.env.DBPASSWORD
    }
    
}

