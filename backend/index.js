const config = require('./config.js'); // contain confidential variable
const express = require("express");
const path = require('path');
const cors = require("cors"); // required cors to handle cors origin requires on our sever
const db = require("./models"); // required database for initalization
const { exceptionHandling } = require('./middleware/exceptionHandling.js');
const { authenticateToken, authorizeDomains } = require('./middleware/userAuth.js');
const { shopifyWebhookHandler} = require('./controllers/webhook-controller.js')

// to inite database 
// @info: pass argument alter and force according requirement 
// @example: { alter: true }
db.sequelize.sync({ alter: true }).then(() => {
	console.log("Database Connected.")
})

// create express app
// @info: you can create multiple app for managaing version
const app = express();

// create middleware in app
app.use(cors()) // cors middleware for cros origin requires
// body parser middleware
app.use(express.urlencoded()); // parse content-type - application/x-www-form-urlencoded @info: argument { extended: true, limit: '50mb' }
app.use(express.json()); // parse requests of content-type - application/json @info: {limit: '50mb'}
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'assets')));
app.use(express.static(path.join(__dirname, 'bundle')));
// Set the views directory
app.set('views', path.join(__dirname, 'bundle', 'app'));

app.use(function (req, res, next) {
	let shop = req.query.shop || req.body?.shop;
	if (shop) {
		res.setHeader('Content-Security-Policy', `frame-ancestors https://${shop} https://admin.shopify.com`)
	}
	next()
})


app.get('/admin/*', (req, res) => {
	let API_KEY = config.SHOPIFY.API_KEY
	return res.render('index', { API_KEY });
});

app.get(['/', '/login'], (req, res) => {
	res.sendFile(path.join(__dirname + '/assets/views/login.html'))
})

app.use('/frontend/*', authorizeDomains)
app.use('/api/*', authenticateToken)
app.post("/webhook", shopifyWebhookHandler);

require("./routes/auth-route")(app)
require("./routes/open-route.js")(app)
require("./routes/post-route.js")(app)
require("./routes/interior-route.js")(app)
require("./routes/store-route.js")(app)
require("./routes/moodboard-route.js")(app)
require("./routes/color-route.js")(app)
require("./routes/order-route.js")(app)


// @info: Error middleware 
app.use(exceptionHandling)


app.get("/error", (req, res) => {
	res.sendFile(path.join(__dirname + `/assets/views/error.html`))
})

// @info: Error if Route not found
app.use((req, res) => {
	res.redirect("/error?code=404")
})

const PORT = config.PORT || 8000;
//Listen PORT
app.listen(PORT, () => {
	console.log(`APP LISTENING ON PORT ${PORT}`);
});
