import { AppProvider } from "@shopify/polaris";
import AppLayout from "./components/layout/AppLayout";
import ShopifyLink from './components/layout/ShopifyLink';
import { BrowserRouter } from 'react-router-dom';
import en from '@shopify/polaris/locales/en.json';
import { Provider } from "react-redux";
import { store } from "./store";

function App() {
  return (
    <AppProvider colorScheme="light"
      linkComponent={ShopifyLink}
      className="height-100"
      i18n={en}
    >
      <Provider store={store}>
        <BrowserRouter>
          <AppLayout />
        </BrowserRouter>
      </Provider>
    </AppProvider>
  )
}

export default App;