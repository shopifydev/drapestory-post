import { configureStore } from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/query'
import { api } from '../api/index'
import toastReducer from '../api/toast/toast-slice'
import storeReducer from '../api/merchantDetail/merchant-slice'

export const store = configureStore({
    reducer: {
        [api.reducerPath]: api.reducer,
        toast: toastReducer,
        store: storeReducer
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({
        serializableCheck: false
      }).concat(api.middleware),
})

setupListeners(store.dispatch)