import { Box, Card, LegacyCard, Tabs } from '@shopify/polaris';
import { useState, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

function TopBarMarkup() {

  const [selected, setSelected] = useState(0);
  let navigate = useNavigate()
  let location = useLocation()
  useEffect(() => {
    setInitialTab()
  }, [])

  const handleTabChange = (selectedTabIndex) => {
    setSelected(selectedTabIndex)
    navigate(tabsList[selectedTabIndex].route)
  }


  const tabsList = [
    {
      id: 'dashboard',
      content: 'Dashboard',
      route: '/admin/dashboard'
    },
    {
      id: 'posts',
      content: 'Posts',
      route: '/admin/posts'
    },
    {
      id: 'interiors',
      content: 'Interiors',
      route: '/admin/inetriors',
    },
    {
      id: 'moodboard',
      content: 'Moodboard',
      route: '/admin/moodboards'
    },
    {
      id: 'colors',
      content: 'Colors',
      route: '/admin/colors'
    },
    {
      id: 'orders',
      content: 'Orders',
      route: '/admin/orders'
    }
  ];

  const setInitialTab = () => {
    let path = location.pathname.split('/')
    path.splice(3, path.length - 3)
    path = path.join("/")
    const selected = tabsList.findIndex(({ route }) => route == path);
    // const selected = tabsList.findIndex(({ route }) => {
    //   let path = location.pathname
    //   if (path.includes("/admin/orders")) path = "/admin/orders"
    //   return route == path
    // });
    if (selected >= 0) {
      setSelected(selected)
    }
  }
  return (
    <div style={{ background: "#e9e9e9", boxShadow: "1px 1px 5px 3px #dddd" }}>
      <Box padding={'3'}>
        <Tabs tabs={tabsList} selected={selected} onSelect={handleTabChange} />
      </Box>
    </div>
  );
}

export default TopBarMarkup;

