import React from "react";
import { Navigation } from '@shopify/polaris';
import { DesktopMajor } from '@shopify/polaris-icons';
import { useLocation } from "react-router-dom";
import { useSelector } from "react-redux";

function NavigationMarkup() {

  const { storeDetails: { is_paid } } = useSelector(state => state.store)
  let location = useLocation();
  const is_disabled = false //is_paid == '0'

  return (
    <Navigation location={location.pathname}>
      <Navigation.Section
        title="Home"
        items={[
          {
            label: "Dashboard",
            url: "/admin/dashboard",
            icon: DesktopMajor,
            disabled: is_disabled
          }
        ]}
      />
    </Navigation>
  )
}

export default NavigationMarkup;