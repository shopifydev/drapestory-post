import React from 'react';
import { Route, Routes } from 'react-router-dom';
import DashboardPage from '../../pages/Dashboard';
import { useSelector } from 'react-redux';
import Posts from '../../pages/Posts';
import Interiors from '../../pages/Interiors';
import Moodboards from '../../pages/Moodboards';
import Colors from '../../pages/Colors';
import Orders from "../../pages/Orders"


function Router() {
  const { storeDetails: { is_paid } } = useSelector(state => state.store)
  const baseurl = '/admin';

  return (
    <Routes>
      <Route exact path={baseurl} element={<DashboardPage />}> </Route>
      <Route exact path={`${baseurl}/dashboard`} element={<DashboardPage />}></Route>
      <Route exact path={`${baseurl}/posts`} element={<Posts />}></Route>
      <Route exact path={`${baseurl}/inetriors`} element={<Interiors />}></Route>
      <Route exact path={`${baseurl}/moodboards`} element={<Moodboards />}></Route>
      <Route exact path={`${baseurl}/colors`} element={<Colors />}></Route>
      <Route exact path={`${baseurl}/orders`} element={<Orders />}></Route>
    </Routes>
  )
}
export default Router;