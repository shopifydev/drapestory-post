import React, { useEffect } from "react";
import { Banner, BlockStack, Box, Spinner, Tabs, Text } from "@shopify/polaris";
import ToastMarkUp from "./../ToastMarkUp";
import { useDispatch, useSelector } from "react-redux";
import { useGetStoreDataMutation } from "../../api/index";
import { useAppBridge } from "@shopify/app-bridge-react";
import { setStoreDetail } from "../../api/storeDetail/store-slice";
import TopBarMarkup from "./TopBarMarkup";
import Router from "./Router";


function AppLayout() {
  let shopify = useAppBridge()
  console.log("shopify", shopify)
  const dispatch = useDispatch();
  const { storeDetails } = useSelector(state => state.store)
  const [getStoreData, getStoreLoading] = useGetStoreDataMutation();


  useEffect(() => {
    setStoreDetailsInState()
  }, [])

  const setStoreDetailsInState = async () => {
    try {
      let storeDetailsResponse = await getStoreData().unwrap()
      console.log("storeDetailsResponse", storeDetailsResponse)
      dispatch(setStoreDetail(storeDetailsResponse.data))
    } catch (error) {
      console.log("Error", error)
      dispatch(setStoreDetail(null))
    }
  }
  const tabs = [
    {
      id: 'all-customers-1',
      content: 'All',
      accessibilityLabel: 'All customers',
      panelID: 'all-customers-content-1',
    },
    {
      id: 'accepts-marketing-1',
      content: 'Accepts marketing',
      panelID: 'accepts-marketing-content-1',
    },
  ];

  return (<>
    {
      getStoreLoading.isError ? (
        <Box padding={"5"}>
          <Banner
            title="It looks like something went wrong!"
            status="warning"
          >
            <Text as="p">Please try again in a few minutes or refresh the page now.</Text>
          </Banner>
        </Box>
      ) : (
        !storeDetails ? (
          <div className="app-spinner" style={{ position: 'fixed', top: "50%", left: "50%" }}><Spinner accessibilityLabel="Spinner example" size="large" /></div>
        ) : (
          <>
            <TopBarMarkup />
            <Router />
            <ToastMarkUp />
          </>
        )
      )
    }
  </>
  );
}

export default AppLayout;