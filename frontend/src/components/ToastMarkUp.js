import { Toast } from "@shopify/polaris";
import { openToast,closeToast} from "../api/toast/toast-slice";
import { useDispatch, useSelector } from "react-redux";

function ToastMarkUp() {
  const {message,isActive,isError}  = useSelector(state=>state.toast)
  const dispatch = useDispatch();
  return (
    isActive ?
    <div>
      <Toast content={message} error={isError} onDismiss={()=>{dispatch(closeToast())} }/>
    </div>
      : null
  )
}

export default ToastMarkUp;