import React, { useEffect, useState } from 'react';
import { Button, Popover, Card, Scrollable, FormLayout, Select, TextField, DatePicker, InlineError, Box, InlineStack, Text } from '@shopify/polaris';
import moment from 'moment';
import { CalendarIcon } from '@shopify/polaris-icons';

const datePickerOptionsForDashboard = [
	{ label: 'Custom', value: 'custom' },
	{ label: 'Today', value: 'today' },
	{ label: 'Yesterday', value: 'yesterday' },
	{ label: 'Last 7 days', value: 'last7day' },
	{ label: 'Last 30 days', value: 'last30days' },
	{ label: 'Last 90 days', value: 'last90days' },
	{ label: 'Last month', value: 'lastmonth' },
	{ label: 'Last year', value: 'lastyear' },
	{ label: 'Week to date', value: 'weektodate' },
	{ label: 'Month to date', value: 'monthtodate' },
	{ label: 'Year to date', value: 'yeartodate' },
];

const momentDate = moment();
const s_date = moment().startOf('M')
const e_date = moment().toDate()

export default function CustomDatePicker({ onChange }) {
    const [startDate, setStartDate] = useState(moment(s_date))
    const [endDate, setEndDate] = useState(moment(e_date))
    const [month, setMonth] = useState(moment(startDate).month())
    const [year, setYear] = useState(moment(endDate).year())
    const [selectedDate, setSelectedDate] = useState({
        start: moment(startDate).toDate(),
        end: moment(endDate).toDate()
    })
    const [selectedDateText, setSelectedDateText] = useState({
        start: moment(startDate).format('YYYY-MM-DD'),
        end: moment(endDate).format('YYYY-MM-DD')
    })
    const [active, setActive] = useState(false)
    const [RangeOption, setRangeOption] = useState("last30days")
    const [isStartDateError, setIsStartDateError] = useState(false);
    const [isEndDateError, setIsEndDateError] = useState(false);

    useEffect(() => {
        onChange(selectedDateText.start, selectedDateText.end);
    }, [])

    const handleMonthChange = (month, year) => {
        setMonth(month)
        setYear(year)
    }

    const handleChangeRangeOption = (RangeOption) => {
        let { start, end } = selectedDate;
        let tempSelectedDateText = selectedDateText;
        switch (RangeOption) {
            case 'today':
                start = moment().toDate()
                end = moment().toDate();
                break;
            case 'yesterday':
                start = moment().subtract(1, 'days').toDate()
                end = moment().subtract(1, 'days').toDate()
                break;
            case 'last7day':
                start = moment().subtract(6, 'days').toDate()
                end = moment().toDate();
                break;
            case 'last30days':
                start = moment().subtract(30, 'days').toDate()
                end = moment().toDate();
                break;
            case 'last90days':
                start = moment().subtract(90, 'days').toDate()
                end = moment().toDate();
                break;
            case 'lastmonth':
                start = moment().subtract(1, 'month').startOf('month').toDate()
                end = moment().subtract(1, 'month').endOf('month').toDate();
                break;
            case 'lastyear':
                start = moment().subtract(1, 'year').startOf('year').toDate()
                end = moment().subtract(1, 'year').endOf('year').toDate();
                break;
            case 'weektodate':
                start = moment().weekday(1).toDate()
                end = moment().toDate();
                break;
            case 'monthtodate':
                start = moment().startOf('month').toDate()
                end = moment().toDate();
                break;
            case 'yeartodate':
                start = moment().startOf('year').toDate()
                end = moment().toDate();
                break;
        }

        tempSelectedDateText.start = moment(start).format('YYYY-MM-DD');
        tempSelectedDateText.end = moment(end).format('YYYY-MM-DD');
        setRangeOption(RangeOption)
        setSelectedDate({ start: start, end: end })
        setSelectedDateText(tempSelectedDateText)
    }

    const handleChangeDate = (selectedDate) => {

        var tempSelectedDateText = selectedDateText;

        if (moment(selectedDate.start).isValid()) {
            tempSelectedDateText.start = moment(selectedDate.start).format('YYYY-MM-DD');
            setIsStartDateError(false);
        }
        if (moment(selectedDate.end).isValid()) {
            tempSelectedDateText.end = moment(selectedDate.end).format('YYYY-MM-DD');
            setIsEndDateError(false);
        }
        setSelectedDateText(tempSelectedDateText)
        setSelectedDate(selectedDate)
        setRangeOption("custom")
    }

    const handleChangeDateInput = (input, value) => {

        try {
            let tempSelectedDateText = { ...selectedDateText };
            let tempSelectedDate = { ...selectedDate };
            let error = false
            if (input === 'start') {
                tempSelectedDateText.start = value;
                if (value && moment(value).isValid()) {
                    let d1 = moment(value);
                    let d2 = moment(moment()).format('YYYY-MM-DD');
                    let dff = d1.diff(d2, 'days');
                    if (dff <= 0) {
                        tempSelectedDate.start = moment(value).toDate()
                        setIsStartDateError(false)
                    } else {
                        error = true
                        setIsStartDateError('Start date is not valid')
                    }
                } else {
                    error = true
                    setIsStartDateError('Invailed date');
                }
            }
            if (input === 'end') {
                if (value && moment(value).isValid()) {
                    let d1 = moment(value);
                    let d2 = moment(tempSelectedDate.start).format('YYYY-MM-DD');
                    var d3 = moment(moment()).format('YYYY-MM-DD');

                    let dff = d1.diff(d2, 'days');
                    let dff2 = d1.diff(d3, 'days');
                    if (dff >= 0 && dff2 <= 0) {
                        tempSelectedDate.end = moment(value).toDate();
                        setIsEndDateError(false);
                    } else {
                        error = true
                        setIsEndDateError('End date is not valid');
                    }
                } else {
                    error = true
                    setIsEndDateError('Invailed date');
                }
                tempSelectedDateText.end = value;
            }
            if (!error) {
                setIsEndDateError(false);
                setIsStartDateError(false)
            }
            setSelectedDateText({ ...tempSelectedDateText })
            setSelectedDate({ ...tempSelectedDate })
            setRangeOption("custom")
        } catch (error) {
            console.log(error)
            setSelectedDateText({ ...selectedDateText })
            setSelectedDate({ ...selectedDate })
        }
    }

    const handleApplyFilter = () => {
        try {
            togglePopover();
            const { start, end } = selectedDate;
            const s_Date = moment(start).format('YYYY-MM-DD');
            const e_Date = moment(end).format('YYYY-MM-DD');
            setStartDate(moment(start))
            setEndDate(moment(end))
            handleChangeDate(selectedDate)
            onChange(s_Date, e_Date);
        } catch (error) {
            onChange(s_date, e_date);
            console.log('apply error', error)
        }
    }


    const togglePopover = () => {
        setActive(!active)
    }

    return (
            <div style={{ width: active ? "400px" : "100%", maxWidth: "700px" }}>

                <Popover

                    fluidContent={false}
                    active={active}
                    activator={(
                        <InlineStack align='start'>
                            <Button onClick={togglePopover} size='large' icon={CalendarIcon}>
                                <Text variant='headingMd' >
                                    {moment(selectedDate.start).format('DDMMMYYYY') == moment(selectedDate.end).format('DDMMMYYYY')
                                        ? moment(selectedDate.start).format('DD MMM YYYY')
                                        : moment(selectedDate.start).format('DD MMM YYYY') + " - " + moment(selectedDate.end).format('DD MMM YYYY')
                                    }
                                </Text>
                            </Button>
                        </InlineStack>
                    )}
                    onClose={togglePopover}
                    fullWidth
                    icon={CalendarIcon}
                    footerContent
                    preferredAlignment='left'
                    activatorWrapper='div'
                // preferredPosition='below'
                // preventCloseOnChildOverlayClick
                >
                    <Card padding={'300'}>
                        <Scrollable shadow style={{ height: '300px' }}>
                            <Box padding={'200'}>
                                <FormLayout>
                                    <FormLayout.Group>
                                        <Select label="Date range" options={datePickerOptionsForDashboard} onChange={handleChangeRangeOption} value={RangeOption} />
                                    </FormLayout.Group>
                                    <FormLayout.Group condensed>
                                        <TextField type="text" value={selectedDateText.start} label="Starting" onChange={(event) => handleChangeDateInput("start", event)} />
                                        <TextField type="text" value={selectedDateText.end} label="Ending" disabled={Boolean(isStartDateError)} onChange={(event) => handleChangeDateInput("end", event)} />
                                    </FormLayout.Group>
                                    {Boolean(isStartDateError || isEndDateError) ? (<InlineError message={isStartDateError || isEndDateError} fieldID="Ending" />) : null}
                                    <FormLayout.Group>
                                        <DatePicker
                                            month={month}
                                            year={year}
                                            disableDatesAfter={momentDate.toDate()}
                                            onChange={handleChangeDate}
                                            onMonthChange={handleMonthChange}
                                            selected={selectedDate}
                                            allowRange
                                            multiMonth
                                        />
                                    </FormLayout.Group>
                                </FormLayout>
                            </Box>
                        </Scrollable>

                        <Box padding={'300'}>
                            <div style={{ textAlign: "right" }}>
                                <Button onClick={togglePopover}>Cancel</Button>
                                &nbsp;&nbsp;
                                <Button variant='primary' disabled={Boolean(isStartDateError || isEndDateError)} onClick={handleApplyFilter}>Apply</Button>
                            </div>
                        </Box>
                    </Card>
                </Popover>
            </div>
    );
}