import { Redirect } from "@shopify/app-bridge/actions";
import { createApp } from "@shopify/app-bridge"

let search = window.location.search;
let params = new URLSearchParams(search);
const host = params.get('host');
export const shopConfig = { apiKey: window.API_KEY, forceRedirect: true, host: host };
export const app = createApp(shopConfig);


export const redirect = Redirect.create(app);
export const handleProductRedirect = (product_id, variant_id) => {
	let redirect_url = variant_id ? `/products/${product_id}/variants/${variant_id}` : `/products/${product_id}`
	redirect.dispatch(Redirect.Action.ADMIN_PATH, { path: redirect_url, newContext: true })
}
export const handleOrderRedirect = (order_id) => {
	redirect.dispatch(Redirect.Action.ADMIN_PATH, { path: `/orders/${order_id}`, newContext: true })
}

const _pagination = {
	hasNext: false,
	hasPrevious: false,
	page: 1,
	perpage: 20,
	showing: null,
	total: 0,
	totalpages: 0
}

export const setPagination = function (page = _pagination.page, perpage = _pagination.perpage, totalpages = 0, total = 0) {
	let pagination = _pagination;
	const from = ((page * perpage) - perpage) + 1;
	const to = totalpages === page ? total : perpage * page;
	const hasPrevious = page > 1;
	const hasNext = totalpages > page;
	let showing = total > 0 ? `Showing ${from} to ${to} of ${total} entries` : '';
	pagination = { ...pagination, hasNext: hasNext, hasPrevious: hasPrevious, page: page, total, perpage: perpage, totalpages: totalpages, showing: showing }
	return pagination;
}

export const datePickerOptionsForDashboard = [
	{ label: 'Custom', value: 'custom' },
	{ label: 'Today', value: 'today' },
	{ label: 'Yesterday', value: 'yesterday' },
	{ label: 'Last 7 days', value: 'last7day' },
	{ label: 'Last 30 days', value: 'last30days' },
	{ label: 'Last 90 days', value: 'last90days' },
	{ label: 'Last month', value: 'lastmonth' },
	{ label: 'Last year', value: 'lastyear' },
	{ label: 'Week to date', value: 'weektodate' },
	{ label: 'Month to date', value: 'monthtodate' },
	{ label: 'Year to date', value: 'yeartodate' },
];
export function isEmpty(value) {
	if (Array.isArray(value)) {
		return value.length === 0;
	} else {
		return value === '' || value == null;
	}
}