let moment = require('moment');

export const offerPriceCalculation = (original_price, discount_price, discountType) => {
  original_price = Number(original_price)
  discount_price = Number(discount_price)
  if (discountType == 'percentage') {
    return (original_price - (original_price * (discount_price / 100))).toFixed(2)
  } else {
    return (original_price - discount_price).toFixed(2)
  }

}

export function dateConversion(datetime) {
  const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
  ];
  const date = new Date((new Date(datetime).toISOString()));
  let hours = date.getHours();
  let hourChange = hours > 12 ? hours - 12 : hours

  let ampm = hours >= 12 ? 'PM' : 'AM';
  let dateConvert =
    ("00" + (date.getDate())).slice(-2)
    + " " + ("00" + monthNames[(date.getMonth())]).slice(-3)
    + " " + date.getFullYear() + ", "
    + hourChange + ":"
    + ("00" + date.getMinutes()).slice(-2) + ' ' + ampm;

  let dateChange = moment(dateConvert).fromNow();
  return dateChange;
}

export function dateConvert(datetime) {
  let date = moment(datetime).format('lll');
  return date
}

export function amountSeparator(amount, isCompact = false) {
  if (amount) {
    if (typeof amount != 'number') {
      amount = Number(amount) ? Number(amount) : 0
    }
    if (isCompact) {
      amount = Intl.NumberFormat("en", {
        currency: "INR",
        notation: 'compact'
      }).format(amount)
    } else {
      amount = Intl.NumberFormat("en-IN", {
        currency: "INR",
      }).format(amount)
    }
    return amount
  } else {
    return 0
  }
}

export function ordinal_suffix_of(amount) {
  if (typeof amount != 'number') {
    amount = Number(amount) ? Number(amount) : 0
  }
  var j = amount % 10,
      k = amount % 100;
  if (j == 1 && k != 11) {
      return amount + "st";
  }
  if (j == 2 && k != 12) {
      return amount + "nd";
  }
  if (j == 3 && k != 13) {
      return amount + "rd";
  }
  return amount + "th";
}

export function apiLoading(apiData) {
  return Boolean(apiData.status == 'pending' || apiData.status == 'uninitialized')
}

export function typeOfSelectedfile(file) {
  let path = file.split('.')
  return path[path.length - 1]
}