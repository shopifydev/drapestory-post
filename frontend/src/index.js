import React from 'react';
import ReactDOM from 'react-dom/client';
import "@shopify/polaris/build/esm/styles.css";  // This is polaris css as per @shopify/polaris version  
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
