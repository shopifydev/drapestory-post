import { useAppBridge } from "@shopify/app-bridge-react";
import { Box, Card, IndexFilters, IndexTable, Page, Text, useSetIndexFiltersMode, IndexFiltersMode, ButtonGroup, Button, Popover, ResourceList, ResourceItem, Thumbnail, InlineStack, OptionList, RadioButton } from "@shopify/polaris";
import { useEffect, useRef, useState } from "react";
import { useGetMoodboardApiMutation, useExportMoodboardApiMutation } from "../api";
import { handleProductRedirect, setPagination } from "../config/setting";
import CustomDatePicker from "../components/CustomDatePicker";
import { debounce } from "lodash";


let pagination = setPagination()
function Moodboards() {
  let shopify = useAppBridge();
  let [moodboards, setMoodboards] = useState([]);
  let [search, setSearch] = useState("")
  let [getMoodboardApi, getMoodboardApiStatus] = useGetMoodboardApiMutation()
  const [exportMoodboardApi, exportMoodboardApiStatus] = useExportMoodboardApiMutation()
  let { mode, setMode } = useSetIndexFiltersMode(IndexFiltersMode.Filtering)
  const delayedSearch = useRef(debounce((a) => getAllMoodboards(a), 500)).current;

  const [popoverActive, setPopoverActive] = useState(false);
  const [dates, setDates] = useState({});
  const [exportOption, setExportOption] = useState('exportAll');



  useEffect(() => {
    getAllMoodboards()
  }, [])



  const getAllMoodboards = async (a = search, dates) => {
    try {
      let payload = {
        limit: pagination.perpage,
        page: pagination.page,
        search: a,
        dates
      }
      let { data, total, total_pages } = await getMoodboardApi(payload).unwrap()
      setMoodboards(data);
      pagination = setPagination(pagination.page, pagination.perpage, total_pages, total)
    } catch (error) {
      console.log(error)
      shopify.toast.show(error?.data?.message || 'Something went wrong', { isError: true });
    }
  }

  const changePage = (page) => {
    if (page >= 1 && page <= pagination.totalpages) {
      pagination.page = page
      getAllMoodboards()
    }
  }

  const togglePopoverActive = (key = false) => {
    setPopoverActive(key)
  }

  const renderItem = ({ node }) => {
    let arr = node.id.split("/")
    let item_id = arr[arr.length - 1]
    return (
      <ResourceItem
        id={item_id}
        media={<Thumbnail
          source={node?.featuredImage?.src}
          size="small"
          alt="1"
        />}
        disabled
      >
        <Text variant="bodyMd" fontWeight="bold" as="h3">
          {node.title}
        </Text>
        <Button variant="plain" onClick={() => { handleProductRedirect(item_id) }}> {item_id}   </Button>
      </ResourceItem>
    )
  }

  const rowMarkup = () => {
    return moodboards.map((item, index) => {
      return (
        <IndexTable.Row
          id={item.id}
          key={index}
          position={index}
        >
          <IndexTable.Cell>{item?.title || ""}</IndexTable.Cell>
          <IndexTable.Cell>
            <Text variant="bodyMd" fontWeight="bold" as="span">
              {item?.customer?.first_name || ""} {item?.customer?.last_name || ""}
            </Text>
          </IndexTable.Cell>
          <IndexTable.Cell>{item?.customer?.phone || ""}</IndexTable.Cell>
          <IndexTable.Cell>{item?.customer?.email || ""}</IndexTable.Cell>
          <IndexTable.Cell>{item?.description || ""}</IndexTable.Cell>
          <IndexTable.Cell>
            <Popover
              sectioned
              active={popoverActive == `product${index}`}
              activator={<Button variant="tertiary" onClick={() => { togglePopoverActive(`product${index}`) }}>{item?.products?.length || 0}</Button>}
              onClose={togglePopoverActive}
              ariaHaspopup={false}
            >
              <Popover.Pane>
                <ResourceList items={item.products} renderItem={renderItem} />
              </Popover.Pane>
            </Popover>
          </IndexTable.Cell>
          <IndexTable.Cell>
            <InlineStack gap={"050"}>
              {
                item?.selected_colors?.map((color) => (
                  <div style={{ background: `${color.code}`, width: "20px", height: "20px", border: "1px solid black", borderRadius: "50%" }}></div>
                ))
              }
            </InlineStack>
          </IndexTable.Cell>
        </IndexTable.Row>
      )
    })
  }

  const handleQuerySearch = (value) => {
    pagination.page = 1
    setSearch(value);
    delayedSearch(value)
  }

  useEffect(() => {
    delayedSearch({ search: search, dates: dates });
  }, [search, pagination.page, dates]);

  const onChangeDates = (start_date, end_date) => {
    setDates({ start: start_date, end: end_date });
  };

  const downloadMoodboardFile = async () => {
    try {
      let exportAll = exportOption === "exportAll";
      console.log(exportAll, "exportOptionexportOption");
      let payload = {
        limit: pagination.perpage,
        page: pagination.page,
        search: search,
        exportAll: exportAll,
        dates: dates
      }
      console.log(payload, "Ss");
      const response = await exportMoodboardApi(payload).unwrap();

      if (response) {
        const downloadUrl = window.URL.createObjectURL(new Blob([response]));
        const link = document.createElement("a");
        link.href = downloadUrl;
        const name = "moodboard";
        link.setAttribute("download", name + ".csv");
        document.body.appendChild(link);
        link.click();
        link.remove();
        activeTostMessage("Exported successfully");
      } else {
        activeTostMessage("Export Failed", true);
      }
    } catch (error) {
      console.log(error);
    }
  };
  function handelSelectChange(newValue) {
    setExportOption(newValue);
  }



  return (
    <Page fullWidth title="Moodboards">
      <Box paddingBlockEnd="400">
        <div style={{ display: 'flex', justifyContent :"space-between"  }}>
          <CustomDatePicker onChange={onChangeDates} />
          <div style={{ display: 'flex', gap : "0.5rem", justifyContent: 'flex-end' }}>
            <RadioButton
              label="Export with Date Filter"
              checked={exportOption === 'dateFilter'}
              id="dateFilter"
              name="export"
              onChange={() => handelSelectChange('dateFilter')}
            />
            <RadioButton
              label="Export All"
              id="exportAll"
              name="export"
              checked={exportOption === 'exportAll'}
              onChange={() => handelSelectChange('exportAll')}
            />
            <Button onClick={downloadMoodboardFile} primary>
              Export CSV
            </Button>
          </div>
        </div>

       
        <Card>
            <IndexFilters
              queryValue={search}
              queryPlaceholder="Searching in all"
              onQueryChange={handleQuerySearch}
              onQueryClear={() => { handleQuerySearch("") }}
              tabs={[]}
              selected={-1}
              mode={mode}
              setMode={setMode}
              hideFilters={true}
            />
          <IndexTable
            resourceName={{
              singular: 'Moodboard',
              plural: 'Moodboards',
            }}
            itemCount={moodboards.length}
            bulkActions={[
              {
                content: 'Export CSV',
                onAction: () => { downloadMoodboardFile() },
              }
            ]}
            headings={[
              { title: 'Title' },
              { title: 'Name' },
              { title: 'Email' },
              { title: 'Phone' },
              { title: 'Discription', },
              { title: 'Products' },
              { title: 'Selected Colors' }
            ]}
            pagination={{
              hasNext: pagination.hasNext,
              hasPrevious: pagination.hasPrevious,
              onNext: () => { changePage(pagination.page + 1) },
              onPrevious: () => { changePage(pagination.page - 1) },
              label: pagination.showing
            }}
            selectable={false}
            loading={getMoodboardApiStatus.isLoading}
          >
            {rowMarkup()}
          </IndexTable>
        </Card>
      </Box>
    </Page>

  );
}

export default Moodboards;