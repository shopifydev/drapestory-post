import { useAppBridge } from "@shopify/app-bridge-react";
import { Badge, Box, Card, ChoiceList, IndexFilters, IndexTable, Page, Text, useSetIndexFiltersMode, IndexFiltersMode, ButtonGroup, Button } from "@shopify/polaris";
import { useEffect, useRef, useState } from "react";
import { useGetInteriorApiMutation, useUpdateInteriorApiMutation } from "../api";
import { setPagination } from "../config/setting";
import { debounce } from "lodash";

let pagination = setPagination()
let initFilters = { status: 'all' }
function Interiors() {
  let shopify = useAppBridge();
  let [interiors, setInteriors] = useState([]);
  let [allSeletected, setAllSelected] = useState(false);
  let [selectedInteriors, setSelectedInteriors] = useState([])
  let [filters, setFilters] = useState(initFilters)
  let [search, setSearch] = useState("")
  let [getInteriorApi, getInteriorApiStatus] = useGetInteriorApiMutation()
  let [updateInteriorApi, updateInteriorApiStatus] = useUpdateInteriorApiMutation();
  let { mode, setMode } = useSetIndexFiltersMode(IndexFiltersMode.Filtering)
  const delayedSearch = useRef(debounce((a) => getAllInteriors(a), 500)).current;

  useEffect(() => {
    getAllInteriors()
  }, [filters])

  const handleSelectionChange = (selectionType, isSelecting, selection, _position) => {
    console.log(selectionType, isSelecting, selection, _position)
    console.log(selectedInteriors, allSeletected)
  }

  const getAllInteriors = async (a = search) => {
    try {
      let payload = {
        limit: pagination.perpage,
        page: pagination.page,
        search: a
      }
      if (filters.status != 'all') {
        payload.status = filters.status
      }
      let { data, total, total_pages } = await getInteriorApi(payload).unwrap()
      setInteriors(data || [])
      pagination = setPagination(pagination.page, pagination.perpage, total_pages, total)
    } catch (error) {
      console.log(error)
      shopify.toast.show(error?.data?.message || 'Something went wrong', { isError: true });
    }
  }

  const updateInteriors = async (id, status) => {
    try {
      let payload = {
        id: id,
        updatePayload: {
          status: status
        }
      }
      let { message } = await updateInteriorApi(payload).unwrap()
      shopify.toast.show(message || "Post updated.")
      getAllInteriors()
    } catch (error) {
      shopify.toast.show(error.data.message || "Error to update interior.", { isError: true })
    }
  }

  const handleQuerySearch = (value) => {
    pagination.page = 1
    setSearch(value);
    delayedSearch(value)
  }

  const changePage = (page) => {
    if (page >= 1 && page <= pagination.totalpages) {
      pagination.page = page
      getAllPosts()
    }
  }

  const rowMarkup = () => {
    return interiors.map((item, index) => {
      return (
        <IndexTable.Row
          id={item.id}
          key={index}
          selected={selectedInteriors.includes(item.id)}
          position={index}
        >
          <IndexTable.Cell>
            <Text variant="bodyMd" fontWeight="bold" as="span">
              {item?.first_name || ""} {item?.last_name || ""}
            </Text>
          </IndexTable.Cell>
          <IndexTable.Cell>{item?.email || ""}</IndexTable.Cell>
          <IndexTable.Cell>{item?.phone || ""}</IndexTable.Cell>
          <IndexTable.Cell>{item?.gst_number || ""}</IndexTable.Cell>
          <IndexTable.Cell>{item?.location || ""}</IndexTable.Cell>
          <IndexTable.Cell>{item?.project_handle || ""}</IndexTable.Cell>
          <IndexTable.Cell>{item?.time || ""}</IndexTable.Cell>
          <IndexTable.Cell>{item?.communication_mode || ""}</IndexTable.Cell>
          <IndexTable.Cell>
            {
              item?.status == '1' ?
                <Badge
                  tone="success"
                  progress="complete"
                  toneAndProgressLabelOverride="Status: Published. Post is visible."
                >
                  Published
                </Badge> :
                <Badge
                  tone="critical"
                  progress="incomplete"
                  toneAndProgressLabelOverride="Status:Unpublished.Post is not visible."
                >
                  Unpublished
                </Badge>
            }
          </IndexTable.Cell>
          <IndexTable.Cell>
            <ButtonGroup variant="segmented">
              <Button pressed={item.status == '1'} onClick={() => { updateInteriors(item.id, '1') }} tone="success">
                Published
              </Button>
              <Button pressed={item.status == '0'} onClick={() => { updateInteriors(item.id, '0') }} tone="critical">
                Unpublished
              </Button>
            </ButtonGroup>
          </IndexTable.Cell>
        </IndexTable.Row>
      )
    })
  }
  const handleChangeFilters = (value, filterKey) => {
    setFilters((perv) => {
      perv[filterKey] = value[0]
      return { ...perv }
    })
  }

  const handleFiltersClearAll = () => {
    setFilters({ ...initFilters })
  }

  const appliedFilters = (filterobj) => {
    return Object.keys(filterobj).map((key) => ({
      key,
      label: `Interior ${key}`,
      onRemove: () => {
        setFilters((perv) => {
          perv[key] = initFilters[key] || undefined
          return { ...perv }
        })
      },
    }))
  }


  return (
    <>
      <Page fullWidth title="Interiors">
        <Box paddingBlockEnd="400">
          <Card>
            {interiors.length > 0 &&
              <IndexFilters
                queryValue={search}
                queryPlaceholder="Searching in all"
                onQueryChange={handleQuerySearch}
                onQueryClear={() => { handleQuerySearch("") }}
                filters={[{
                  key: 'status',
                  label: 'Interior status',
                  filter: (
                    <ChoiceList
                      title="Interior status"
                      titleHidden
                      choices={[
                        { label: 'All', value: 'all' },
                        { label: 'Published', value: '1' },
                        { label: 'Unpublished', value: '0' },
                      ]}
                      selected={[filters.status || 'all']}
                      onChange={(selected) => { handleChangeFilters(selected, "status") }}
                    />
                  ),
                  shortcut: true,
                }]}
                appliedFilters={appliedFilters(filters)}
                onClearAll={handleFiltersClearAll}
                tabs={[]}
                selected={-1}
                mode={mode}
                setMode={setMode}

              />}
            <IndexTable
              resourceName={{
                singular: 'Interior',
                plural: 'Interiors',
              }}
              itemCount={interiors.length}
              selectedItemsCount={
                allSeletected ? 'All' : selectedInteriors.length
              }
              promotedBulkActions={[
                {
                  content: 'Export CSV',
                  onAction: () => console.log('Todo: implement payment capture'),
                }
              ]}
              onSelectionChange={handleSelectionChange}
              headings={[
                { title: 'Name' },
                { title: 'Email' },
                { title: 'Phone' },
                { title: 'GST', },
                { title: 'Location' },
                { title: 'Project handle' },
                { title: 'Time' },
                { title: 'Communication mode' },
                { title: 'Status' },
                { title: 'Action' }
              ]}
              pagination={{
                hasNext: pagination.hasNext,
                hasPrevious: pagination.hasPrevious,
                onNext: () => { changePage(pagination.page + 1) },
                onPrevious: () => { changePage(pagination.page - 1) },
                label: pagination.showing
              }}
              selectable={false}
              loading={getInteriorApiStatus.isLoading}
            >
              {rowMarkup()}
            </IndexTable>
          </Card>
        </Box>
      </Page>
    </>
  );
}

export default Interiors;