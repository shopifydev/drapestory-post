import { useAppBridge } from "@shopify/app-bridge-react";
import { Badge, Box, Card, ChoiceList, IndexFilters, IndexTable, Page, Text, Thumbnail, useSetIndexFiltersMode, IndexFiltersMode, ButtonGroup, Button, InlineStack } from "@shopify/polaris";
import { useCallback, useEffect, useRef, useState } from "react";
import { useGetPostApiMutation, useUpdatePostApiMutation } from "../api";
import { setPagination } from "../config/setting";
import { debounce } from "lodash";

let pagination = setPagination()
let initFilters = { status: 'all' }
function Posts() {
  let shopify = useAppBridge();
  let [posts, setPosts] = useState([]);
  let [allSeletected, setAllSelected] = useState(false);
  let [selectedPosts, setSelectedPosts] = useState([])
  let [filters, setFilters] = useState(initFilters)
  let [search, setSearch] = useState("")
  let [updateLoadingIndex, setUpdateLoadingIndex] = useState(null)
  const delayedSearch = useRef(debounce((a) => getAllPosts(a), 500)).current;

  let [getPostApi, getPostApiStatus] = useGetPostApiMutation()
  let [updatePostApi, updatePostApiStatus] = useUpdatePostApiMutation();
  let { mode, setMode } = useSetIndexFiltersMode(IndexFiltersMode.Filtering)
  useEffect(() => {
    getAllPosts()
  }, [filters])


  const handleSelectionChange = (selectionType, isSelecting, selection, _position) => {
    console.log(selectionType, isSelecting, selection, _position)
    console.log(selectedPosts, allSeletected)
  }

  const getAllPosts = async (a = search) => {
    try {
      let payload = {
        limit: pagination.perpage,
        page: pagination.page,
        search: a
      }
      if (filters.status != 'all') {
        payload.status = filters.status
      }
      let { data, total, total_pages } = await getPostApi(payload).unwrap()
      setPosts(data || [])
      pagination = setPagination(pagination.page, pagination.perpage, total_pages, total)
    } catch (error) {
      console.log(error)
      shopify.toast.show(error?.data?.message || 'Something went wrong', { isError: true });
    }
  }

  const updatePost = async (id, status, index) => {
    try {
      let payload = {
        id: id,
        updatePayload: {
          status: status
        }
      }
      let { message } = await updatePostApi(payload).unwrap()
      shopify.toast.show(message || "Post updated.")
      getAllPosts()
      if (index) setUpdateLoadingIndex(index)
    } catch (error) {
      shopify.toast.show(error.data.message || "Error to update post.", { isError: true })
    }
  }

  const showImages = (images = []) => {
    // console.log("images",images)
    if (typeof images == "string") images = JSON.parse(images);
    if (images.length == 0) return <></>
    return <InlineStack gap={"100"} wrap={false}>
      {
        images.map((image, index) => (
          <Thumbnail
            source={image}
            size="small"
            key={index}
            alt={`Post ${index}`}
          />
        ))
      }
    </InlineStack>
  }

  const handleQuerySearch = (value) => {
    pagination.page = 1
    setSearch(value);
    delayedSearch(value)
  }


  const rowMarkup = () => {
    return posts.map((item, index) => {
      return (
        <IndexTable.Row
          id={item.id}
          key={index}
          selected={selectedPosts.includes(item.id)}
          position={index}
        >
          <IndexTable.Cell>
            <Text variant="bodyMd" fontWeight="bold" as="span">
              {item?.customer?.first_name || ""} {item?.customer?.last_name || ""}
            </Text>
          </IndexTable.Cell>
          <IndexTable.Cell>{item?.customer?.phone || ""}</IndexTable.Cell>
          <IndexTable.Cell>
            {item.image ? showImages(item.image) : null}
          </IndexTable.Cell>
          <IndexTable.Cell>{item?.title || ""}</IndexTable.Cell>
          <IndexTable.Cell>
            <div style={{ maxWidth: "200px", whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis" }}>
              {item?.description || ""}
            </div>
          </IndexTable.Cell>
          <IndexTable.Cell>
            {
              item?.status == '1' ?
                <Badge
                  tone="success"
                  progress="complete"
                  toneAndProgressLabelOverride="Status: Published. Post is visible."
                >
                  Published
                </Badge> :
                <Badge
                  tone="critical"
                  progress="incomplete"
                  toneAndProgressLabelOverride="Status:Unpublished.Post is not visible."
                >
                  Unpublished
                </Badge>
            }
          </IndexTable.Cell>
          <IndexTable.Cell>
            <ButtonGroup variant="segmented" >
              <Button pressed={item.status == '1'} onClick={() => { updatePost(item.id, '1') }} tone="success" loading={updatePostApiStatus.isLoading && updateLoadingIndex == index}>
                Published
              </Button>
              <Button pressed={item.status == '0'} onClick={() => { updatePost(item.id, '0') }} tone="critical" loading={updatePostApiStatus.isLoading && updateLoadingIndex == index}>
                Unpublished
              </Button>
            </ButtonGroup>
          </IndexTable.Cell>
        </IndexTable.Row>
      )
    })
  }
  const handleChangeFilters = (value, filterKey) => {
    setFilters((perv) => {
      perv[filterKey] = value[0]
      return { ...perv }
    })
  }

  const handleFiltersClearAll = () => {
    setFilters({ ...initFilters })
  }

  const appliedFilters = (filterobj) => {
    return Object.keys(filterobj).map((key) => ({
      key,
      label: `Post ${key}`,
      onRemove: () => {
        setFilters((perv) => {
          perv[key] = initFilters[key] || undefined
          return { ...perv }
        })
      },
    }))
  }

  const changePage = (page) => {
    if (page >= 1 && page <= pagination.totalpages) {
      pagination.page = page
      getAllPosts()
    }
  }

  return (
    <>
      <Page fullWidth title="Posts">
        <Box paddingBlockEnd="400">
          <Card>
            <IndexFilters
              queryValue={search}
              queryPlaceholder="Searching in all"
              onQueryChange={handleQuerySearch}
              onQueryClear={() => { handleQuerySearch("") }}
              filters={[{
                key: 'status',
                label: 'Post status',
                filter: (
                  <ChoiceList
                    title="Post status"
                    titleHidden
                    choices={[
                      { label: 'All', value: 'all' },
                      { label: 'Published', value: '1' },
                      { label: 'Unpublished', value: '0' },
                    ]}
                    selected={[filters.status || 'all']}
                    onChange={(selected) => { handleChangeFilters(selected, "status") }}
                  />
                ),
                shortcut: true,
              }]}
              appliedFilters={appliedFilters(filters)}
              onClearAll={handleFiltersClearAll}
              tabs={[]}
              selected={-1}
              mode={mode}
              setMode={setMode}

            />
            <IndexTable
              resourceName={{
                singular: 'Post',
                plural: 'Posts',
              }}
              itemCount={posts.length}
              selectedItemsCount={
                allSeletected ? 'All' : selectedPosts.length
              }
              promotedBulkActions={[
                {
                  content: 'Export CSV',
                  onAction: () => console.log('Todo: implement payment capture'),
                }
              ]}
              onSelectionChange={handleSelectionChange}
              headings={[
                { title: 'Name' },
                { title: 'Phone' },
                { title: 'Images' },
                { title: 'Title', },
                { title: 'Description' },
                { title: 'Published' },
                { title: 'Action' }
              ]}
              pagination={{
                hasNext: pagination.hasNext,
                hasPrevious: pagination.hasPrevious,
                onNext: () => { changePage(pagination.page + 1) },
                onPrevious: () => { changePage(pagination.page - 1) },
                label: pagination.showing
              }}
              selectable={false}
              loading={getPostApiStatus.isLoading}
            >
              {rowMarkup()}
            </IndexTable>
          </Card>
        </Box>
      </Page>
    </>
  );
}

export default Posts;