import { BlockStack, Box, Card, Grid, InlineStack, Page, Spinner, Text } from "@shopify/polaris";
import { useEffect, useState } from "react";
import { useGetDashboardDataMutation } from "../api";


function DashboardPage() {
  const [counts, setCounts] = useState({
    posts: 0,
    interiors: 0,
    moodboards: 0,
    colors: 0
  })

  let [getDashboardData, getDashboardDataStatus] = useGetDashboardDataMutation()

  useEffect(() => {
    dashboardData()
  },[])

  const dashboardData = async () => {
    try {
      let { data } = await getDashboardData().unwrap()
      setCounts(data)
    } catch (error) {
      console.log(error)
    }
  }



  return (
    <>
      <Page fullWidth>
        <BlockStack gap="500">
          <Grid>
            <Grid.Cell columnSpan={{ xs: 12, sm: 6, md: 6, lg: 3, xl: 3 }}>
              <Card>
                <BlockStack gap="200">
                  <Text as="h2" variant="headingSm">
                    Posts
                  </Text>
                  {
                    getDashboardDataStatus.isLoading ?
                      <Spinner accessibilityLabel="Spinner example" size="small" />
                      :
                      <div>{counts.posts}</div>
                  }
                </BlockStack>
              </Card>
            </Grid.Cell>
            <Grid.Cell columnSpan={{ xs: 12, sm: 6, md: 6, lg: 3, xl: 3 }}>
              <Card>
                <BlockStack gap="200">
                  <Text as="h2" variant="headingSm">
                    Interiors
                  </Text>
                  {
                    getDashboardDataStatus.isLoading ?
                      <Spinner accessibilityLabel="Spinner example" size="small" />
                      :
                      <div>{counts.interiors}</div>
                  }
                </BlockStack>
              </Card>
            </Grid.Cell>
            <Grid.Cell columnSpan={{ xs: 12, sm: 6, md: 6, lg: 3, xl: 3 }}>
              <Card sectioned>
                <BlockStack gap="200">
                  <Text as="h2" variant="headingSm">
                    Moodboards
                  </Text>
                  {
                    getDashboardDataStatus.isLoading ?
                      <Spinner accessibilityLabel="Spinner example" size="small" />
                      :
                      <div>{counts.moodboards}</div>
                  }
                </BlockStack>
              </Card>
            </Grid.Cell>
            <Grid.Cell columnSpan={{ xs: 12, sm: 6, md: 6, lg: 3, xl: 3 }}>
              <Card title="Colors" sectioned>
                <BlockStack gap="200">
                  <Text as="h2" variant="headingSm">
                    Colors
                  </Text>
                  {
                    getDashboardDataStatus.isLoading ?
                      <Spinner accessibilityLabel="Spinner example" size="small" />
                      :
                      <div>{counts.colors}</div>
                  }
                </BlockStack>
              </Card>
            </Grid.Cell>
          </Grid>
        </BlockStack>
      </Page>
    </>
  );
}

export default DashboardPage;