import { useAppBridge } from "@shopify/app-bridge-react";
import { Box, Card, IndexFilters, IndexTable, Page, Text, useSetIndexFiltersMode, IndexFiltersMode, ButtonGroup, Button, Popover, ResourceList, ResourceItem, Thumbnail, InlineStack, OptionList, RadioButton } from "@shopify/polaris";
import { useEffect, useRef, useState } from "react";
import { useGetOrdersApiMutation } from "../api";
import { handleProductRedirect, setPagination } from "../config/setting";
import { debounce } from "lodash";


let pagination = setPagination()
function Orders() {
  let shopify = useAppBridge();
  let [moodboards, setMoodboards] = useState([]);
  let [allSeletected, setAllSelected] = useState(true);
  let [selectedMoodboards, setSelectedMoodboards] = useState([])
  let [search, setSearch] = useState("");
  const [dates, setDates] = useState({});
  let [getOrderApi, getOrderApiStatus] = useGetOrdersApiMutation()
  let { mode, setMode } = useSetIndexFiltersMode(IndexFiltersMode.Filtering)
  const delayedSearch = useRef(debounce((a) => getAllMoodboards(a), 500)).current;

  const [popoverActive, setPopoverActive] = useState(false);



  useEffect(() => {
    getAllMoodboards()
  }, [])

  const handleSelectionChange = (selectionType, isSelecting, selection, _position) => {
    console.log(selectionType, isSelecting, selection, _position)
    console.log(selectedMoodboards, allSeletected)
  }

  const getAllMoodboards = async (a = search, dates) => {
    try {
      let payload = {
        limit: pagination.perpage,
        page: pagination.page,
        search: a,
        dates
      }
      let { data, total, total_pages } = await getOrderApi(payload).unwrap()
      setMoodboards(data);
      pagination = setPagination(pagination.page, pagination.perpage, total_pages, total)
    } catch (error) {
      console.log(error)
      shopify.toast.show(error?.data?.message || 'Something went wrong', { isError: true });
    }
  }

  const changePage = (page) => {
    if (page >= 1 && page <= pagination.totalpages) {
      pagination.page = page
      getAllMoodboards()
    }
  }

  const togglePopoverActive = (key = false) => {
    setPopoverActive(key)
  }

  const renderItem = ({ node }) => {
    let arr = node.id.split("/")
    let item_id = arr[arr.length - 1]
    return (
      <ResourceItem
        id={item_id}
        media={<Thumbnail
          source={node?.featuredImage?.src}
          size="small"
          alt="1"
        />}
        disabled
      >
        <Text variant="bodyMd" fontWeight="bold" as="h3">
          {node.title}
        </Text>
        <Button variant="plain" onClick={() => { handleProductRedirect(item_id) }}> {item_id}   </Button>
      </ResourceItem>
    )
  }

  const rowMarkup = () => {
    return moodboards.map((item, index) => {
      return (
        <IndexTable.Row
          id={item.id}
          key={item.id}
          position={index}
        >
          <IndexTable.Cell>
            <Text variant="bodyMd" fontWeight="bold" as="span">
              {item?.order_name || ""}
            </Text>
          </IndexTable.Cell>
          <IndexTable.Cell>
            <Text variant="bodyMd" fontWeight="bold" as="span">
              {item?.customer_name || ""}
            </Text>
          </IndexTable.Cell>
          <IndexTable.Cell>{item?.customer_email || ""}</IndexTable.Cell>
          <IndexTable.Cell>{item?.amount || ""}</IndexTable.Cell>
        </IndexTable.Row>
      )
    })
  }

  const handleQuerySearch = (value) => {
    pagination.page = 1
    setSearch(value);
    delayedSearch(value)
  }

  useEffect(() => {
    delayedSearch({ search: search, dates: dates });
    handleSelectionChange("all", false);
  }, [search, pagination.page, dates]);

 
  return (
    <Page fullWidth title="Orders">
      <Box paddingBlockEnd="400">      
        <Card>
            <IndexFilters
              queryValue={search}
              queryPlaceholder="Searching in all"
              onQueryChange={handleQuerySearch}
              onQueryClear={() => { handleQuerySearch("") }}
              tabs={[]}
              selected={-1}
              mode={mode}
              setMode={setMode}
              hideFilters={true}
            />
          <IndexTable
            resourceName={{
              singular: 'Moodboard',
              plural: 'Moodboards',
            }}
            itemCount={moodboards.length}
            selectedItemsCount={
              allSeletected ? 'All' : selectedMoodboards.length
            }

            onSelectionChange={handleSelectionChange}
            headings={[
              { title: 'order_name' },
              { title: 'customer_name' },
              { title: 'customer_email', },
              { title: 'amount' },
            ]}
            pagination={{
              hasNext: pagination.hasNext,
              hasPrevious: pagination.hasPrevious,
              onNext: () => { changePage(pagination.page + 1) },
              onPrevious: () => { changePage(pagination.page - 1) },
              label: pagination.showing
            }}
            selectable={false}
            loading={getOrderApiStatus.isLoading}
          >
            {rowMarkup()}
          </IndexTable>
        </Card>
      </Box>
    </Page>

  );
}

export default Orders;