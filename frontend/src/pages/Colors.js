import { useAppBridge } from "@shopify/app-bridge-react";
import { Page, Text, ResourceList, ResourceItem, Layout, Avatar, Card, Icon, ColorPicker, Button, Form, FormLayout, TextField, hexToRgb } from "@shopify/polaris";
import { useEffect, useState } from "react";
import { useCreateColorApiMutation,useDeleteColorsApiMutation, useGetColorApiMutation } from "../api";
import { DeleteIcon } from '@shopify/polaris-icons';
import { hsbToHex, rgbToHsb } from '@shopify/polaris';

function Colors() {
  let shopify = useAppBridge();
  let [colors, setColors] = useState([]);
  let [selectedIds, setSelectedIds] = useState([])
  let [selectedColor, setSelectedColor] = useState({
    hue: 120,
    brightness: 1,
    saturation: 1,
  })
  let [createdColor, setCreatedColor] = useState({ name: "", code: "" })

  let [getColorApi, getColorApiStatus] = useGetColorApiMutation()
  let [createColorApi, createColorApiStatus] = useCreateColorApiMutation();
  let [deleteColorsApi, deleteColorsApiStatus] = useDeleteColorsApiMutation();


  


  useEffect(() => {
    getAllColors()
  }, [])

  const handleSelectionChange = (array) => {
    setSelectedIds(array);
  }

  const getAllColors = async () => {
    try {
      let { data } = await getColorApi().unwrap();
      if (data?.length > 0) {
        setColors(data); 
      } else {
        setColors([]); 
      }
    } catch (error) {
      console.log(error)
      shopify.toast.show(error?.data?.message || 'Something went wrong', { isError: true });
    }
  }

  const handleChangeCreateColor = (type, value) => {

    if (type == "code") {
      value = value.toLocaleUpperCase()
      let reg = /^#[0-9A-F]*$/
      if (reg.test(value) && value.length <= 7) {
        setCreatedColor((perv) => ({ ...perv, code: value }))
        if (value.length == 6) setSelectedColor(rgbToHsb(hexToRgb(value)))
      }
    } else {
      setCreatedColor((perv) => ({ ...perv, name: value }))
    }
  }

  const handleColorChange = (value) => {
    setSelectedColor(value)
    let hexCode = hsbToHex(value).toLocaleUpperCase()
    setCreatedColor((perv) => ({ ...perv, code: hexCode }))

  }

  const createNewColor = async () => {
    try {
      if (!createdColor.name) return shopify.toast.show('Please add name of color', { isError: true });
      if (!createdColor.code) setCreatedColor((perv) => ({ ...perv, code: hsbToHex(selectedColor) }));

      await createColorApi(createdColor).unwrap();
      getAllColors();
      shopify.toast.show('New color added.');

    } catch (error) {
      console.log(error)
      shopify.toast.show(error?.data?.message || 'Something went wrong', { isError: true });
    }
  }

  
  const handleDeleteColors = async (colorIds) => {
    try {
      if (!Array.isArray(colorIds) || colorIds.length === 0) {
        shopify.toast.show('No colors selected for deletion.', { isError: true });
        return;
      }
        await deleteColorsApi(colorIds).unwrap();
        setSelectedIds([]);
        getAllColors();
      shopify.toast.show('Colors deleted successfully.');
  
    } catch (error) {
  
      const errorMessage = error?.data?.message || 'Something went wrong while deleting colors';
      shopify.toast.show(errorMessage, { isError: true });
    }
  };
  


  return (

    <Page fullWidth title="Colors">
      <Layout>
        <Layout.Section variant="oneHalf">
          <Card >
            <div style={{ maxHeight: "300px", overflowY: "auto", scrollbarWidth: "3px" }}>
              <ResourceList
                loading={getColorApiStatus.isLoading}
                resourceName={{ singular: 'color', plural: 'colors' }}
                items={colors}
                promotedBulkActions={[
                  {
                    onAction: () => { handleDeleteColors(selectedIds)},
                    content: "Delete",
                    icon: <Icon source={DeleteIcon} />,
                    loading: deleteColorsApiStatus 
                  }
                ]}
                onSelectionChange={(item) => { handleSelectionChange(item) }}
                selectedItems={selectedIds}
                renderItem={(item) => {
                  const { id, code, name, image } = item;

                  const media = code ? <div style={{ background: `${code}`, width: "40px", height: "40px", border: "1px solid #D9D9D9", borderRadius: "10%" }}></div> : <Avatar customer size="md" name={name} />;

                  return (
                    <ResourceItem
                      id={id}
                      media={media}
                      accessibilityLabel={`View details for ${name}`}
                    >
                      <Text variant="bodyMd" fontWeight="bold" as="h3">
                        {name}
                      </Text>
                      <div>{code}</div>
                    </ResourceItem>
                  );
                }}
              />
            </div>
          </Card>
        </Layout.Section>
        <Layout.Section>
          <Card>
            <FormLayout>
              <FormLayout.Group>
                <TextField
                  value={createdColor.name}
                  onChange={(value) => { handleChangeCreateColor("name", value) }}
                  label="Name of color"
                />

                <TextField
                  value={createdColor.code}
                  onChange={(value) => { handleChangeCreateColor("code", value) }}
                  label="Code of colors"
                  helpText={
                    <span>
                      {"Write Hex code (#D9F9AC)."}
                    </span>
                  }
                />
              </FormLayout.Group>

              <ColorPicker fullWidth onChange={handleColorChange} color={selectedColor} />

              <Button loading={createColorApiStatus.isLoading} size="large" fullWidth tone="success" variant="primary" onClick={() => { createNewColor() }}>Create Color</Button>
            </FormLayout>
          </Card>
        </Layout.Section>
      </Layout>
    </Page>

  );
}

export default Colors;