import { createSlice } from '@reduxjs/toolkit'
const initialState = {
    storeDetails: null
    // pricingPlanModalProps: null
}

export const storeSlice = createSlice({
    name: 'store',
    initialState,
    reducers: {
        setStoreDetail(state, action) {
            state.storeDetails = action.payload;
        },
        // setPricingPlanModalProps(state, action) {
        //     state.pricingPlanModalProps = action.payload
        // }
    }
})

export const { setStoreDetail } = storeSlice.actions;  // add setPricingPlanModalProps
export default storeSlice.reducer