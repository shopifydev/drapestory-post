import { createSlice } from '@reduxjs/toolkit'
const initialState = {
    storeDetails: null
}

export const storeSlice = createSlice({
    name: 'store',
    initialState,
    reducers: {
        setStoreDetail(state, action) {
            state.storeDetails = action.payload;
        }
    }
})

export const { setStoreDetail } = storeSlice.actions;
export default storeSlice.reducer