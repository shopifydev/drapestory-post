import { app } from '../config/setting';
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

import { getSessionToken } from "@shopify/app-bridge";

const baseQuery = fetchBaseQuery({
  baseUrl: `/api`,

  prepareHeaders: async (headers, { }) => {
    try {
      const token = await getSessionToken(app);
      headers.set("authorization", `Bearer ${token}`);
      return headers;
    } catch (err) {
      console.log("Error fetchBaseQuery prepareHeaders--------", err);
    }
  },
});
const baseQueryErrHandler = async (args, api, extraOptions) => {
  let result = await baseQuery(args, api, extraOptions);
  if (result.error && result.error.status == 302) {
    window.location.href = '/login';
  }
  return result
}
export const api = createApi({
  baseQuery: baseQueryErrHandler,
  endpoints: (builder) => ({
    getStoreData: builder.mutation({
      query: () => ({ url: `/store-details`, method: "GET" }),
    }),
    getDashboardData: builder.mutation({
      query: () => ({ url: `/dashboard-details`, method: "GET" }),
    }),
    getPostApi: builder.mutation({
      query: (data) => ({
        url: `/get-all-posts`,
        method: "POST",
        body: data,
      }),
    }),
    updatePostApi: builder.mutation({
      query: (data) => ({
        url: `/update-post/${data.id}`,
        method: "POST",
        body: data.updatePayload
      })
    }),
    getInteriorApi: builder.mutation({
      query: (data) => ({
        url: `/get-all-interior`,
        method: "POST",
        body: data
      })
    }),
    updateInteriorApi: builder.mutation({
      query: (data) => ({
        url: `/update-interior/${data.id}`,
        method: "POST",
        body: data.updatePayload
      })
    }),
    getMoodboardApi: builder.mutation({
      query: (data) => ({
        url: `/all-moodboards`,
        method: "POST",
        body: data
      })
    }),
    exportMoodboardApi: builder.mutation({
      query: (data) => ({
        url: `/export-moodboard`,
        method: "POST",
        body: data,
        responseHandler: async (response) => {
          if (response.status == 200) return await response.blob();
          return response.json();
        },
      })
    }),
    getMoodboardProductsApi: builder.mutation({
      query: (data) => ({
        url: `/moodboard/products`,
        method: "GET",
        query: data
      })
    }),
    getColorApi: builder.mutation({
      query: (data) => ({
        url: `/colors`,
        method: "GET",
      })
    }),
    createColorApi: builder.mutation({
      query: (data) => ({
        url: `/create-color`,
        method: "POST",
        body: data
      })
    }),
    deleteColorsApi: builder.mutation({
      query: (data) => ({
        url: `/delete-colors`, 
        method: "DELETE",
        body: data 
      })
    }),
    getOrdersApi: builder.mutation({
      query: (data) => ({
        url: `/orders`,
        method: "POST",
        body: data
      })
    }),
    
  })
})

// Export hooks for usage in functional components
export const {
  useGetStoreDataMutation,
  useGetDashboardDataMutation,
  useGetPostApiMutation,
  useUpdatePostApiMutation,
  useGetInteriorApiMutation,
  useUpdateInteriorApiMutation,
  useGetMoodboardApiMutation,
  useExportMoodboardApiMutation,
  useGetMoodboardProductsApiMutation,
  useGetColorApiMutation,
  useCreateColorApiMutation,
  useDeleteColorsApiMutation,
  useGetOrdersApiMutation
} = api
