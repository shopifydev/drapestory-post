const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const outputDirectory = path.join(path.dirname(__dirname), './backend/bundle');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env, arg) => {

    const config = {

        entry: { app: './src/index.js' },
        output: {
            path: outputDirectory,
            filename: '[name]/bundle.js',
            publicPath: `/`
        },
        module: {
            rules: [{
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        // options: {
                        //   // you can specify a publicPath here
                        //   // by default it use publicPath in webpackOptions.output
                        //   publicPath: __dirname
                        // }
                    },
                    "css-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8192,
                        },
                    },
                ],

            },
            {
                test: /\.ejs$/,
                use: [
                    {
                        loader: 'ejs-loader',
                        options: {
                            esModule: false
                        },
                    },
                ],

            }
            ]
        },
        resolve: {
            extensions: ['.*', '.js', '.jsx']
        },
        devServer: {
            port: 3000,
            open: true,
            historyApiFallback: true,
            proxy: {
                '/api': 'http://localhost:8000'
            }
        },
        plugins: [
            // new CleanWebpackPlugin([outputDirectory]),
            new MiniCssExtractPlugin({
                // Options similar to the same options in webpackOptions.output
                // both options are optional
                filename: "[name]/bundle.css"
            }),
            new CleanWebpackPlugin({
                cleanOnceBeforeBuildPatterns: ['./js/bundle/*', './css/bundle/*'],
                cleanStaleWebpackAssets: false //remove at the time of live
            }),
            new HtmlWebpackPlugin({
                title: 'app',
                filename: './app/index.ejs',
                favicon: './public/favicon.ico',
                template: './public/index.ejs',
                chunks: ['app'],
                templateParameters: {
                    API_KEY: "<%= API_KEY %>"
                }
            })
        ]
    }
    return config
};
